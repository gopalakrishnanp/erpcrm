/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myntra.commons.excel.test;

import com.myntra.commons.excel.exceptions.ExcelPOIWrapperException;
import com.myntra.commons.excel.poiImpl.MyntraExcelWriter;
import com.myntra.commons.excel.poiImpl.MyntraSheetWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author paroksh
 */
public class WriteDropDown {
    public static void main(String[] args) throws FileNotFoundException, ExcelPOIWrapperException, IOException{
        //newDropDown();
        //oldDropDown();
        hiddenDD();
    }
    
    public static void hiddenDD() throws FileNotFoundException, ExcelPOIWrapperException, IOException{
        FileOutputStream f = new FileOutputStream("/home/paroksh/myntra/CMS/paroTesting/hiddenDD.xlsx");
        MyntraExcelWriter excelWriter = new MyntraExcelWriter(f);
        MyntraSheetWriter sheet = excelWriter.createSheetWriter("dropdown");
        
        List<String> countryName = new ArrayList<String>();
        countryName.add("India");
        countryName.add("Argentina");
        countryName.add("Indonesia");
        
        String name = "par";
        //sheet.writeDropDown(name, countryName, null);
        sheet.writeNamedDropDown(name, countryName, null);
        String name1 = "par1";
        List<String> countryName1 = new ArrayList<String>();
        countryName1.add("x");
        countryName1.add("y");
        countryName1.add("z");
        
        //sheet.writeDropDown(name, countryName1, null);
        //sheet.writeDropDown(name, countryName, null);
        //sheet.writeDropDown(name, "dfv");
        
        excelWriter.save();
    }
    public static void newDropDown()throws FileNotFoundException, ExcelPOIWrapperException, IOException{
        FileOutputStream f = new FileOutputStream("/home/paroksh/myntra/CMS/paroTesting/dropdownRange.xlsx");
        MyntraExcelWriter excelWriter = new MyntraExcelWriter(f);
        MyntraSheetWriter sheet = excelWriter.createSheetWriter("dropdown");
        MyntraSheetWriter hidden = excelWriter.createSheetWriter("dropdown1");
        
        String countryName[] = new String[3];
        countryName[0] = "India";
        countryName[1] = "Australia";
        countryName[2] = "Argentina";
        String val = "DDValue";
        int len = 200;
        for(int i=0;i<len;i++){
            hidden.writeText(val+i);
        }
//        for (int i = 0, length= countryName.length; i < length; i++) {
//           String name = countryName[i];
//           hidden.writeText(name);
//         }
        hidden.setSheetHidden();
        String name = "par";
        hidden.setNamedRange(name, -len-1, 1);
        //sheet.writeDropDown(name, "dfv");
        
        excelWriter.save();
    }
    
    public static void oldDropDown()throws FileNotFoundException, ExcelPOIWrapperException, IOException{
        FileOutputStream f = new FileOutputStream("/home/paroksh/myntra/CMS/paroTesting/dropdown.xlsx");
        MyntraExcelWriter excelWriter = new MyntraExcelWriter(f);
        MyntraSheetWriter sheet = excelWriter.createSheetWriter("dropdown");
        MyntraSheetWriter sheet1 = excelWriter.createSheetWriter("dropdown1");
        
        List<String> lst = new ArrayList<String>();
        String val = "DDValue";
        for(int i=0;i<100;i++){
            lst.add(val+i);
        }
        System.out.println(lst.size());
        //sheet.nextCell();sheet.nextCell();
        sheet.setColumnMode();
        //sheet.nextCell();sheet.nextCell();sheet.nextCell();sheet.nextCell();
//        try{
//        //sheet.writeDropDownCells(lst, lst.get(0), 1, 1);
//        }
//        catch(ExcelPOIWrapperException e){
//            System.out.println(e);
//        }
        
        //sheet.writeCellComment("chk");
//        for(int i=0;i<2;i++){
//            sheet1.writeDropDown(lst, null);
//            sheet.writeDropDown(lst, null);
//            //String x = "paroksh";
//            //sheet.setNamedCell(x);
//        }
        excelWriter.save();
        
    }
    
}

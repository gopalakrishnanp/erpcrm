package com.myntra.commons.excel;

import com.myntra.commons.excel.enums.CellType;
import com.myntra.commons.excel.exceptions.ExcelPOIWrapperException;
import java.util.Date;

public interface MyntraSheetReaderInterface {

    /**
     * variables to denote string "row" and "columns" to be used by
     * implementation class to check what is the current mode of reading
     */
    public static final String MODE_ROW = "row";
    public static final String MODE_COLUMN = "column";

    /**
     * @return String Name of the sheet
     */
    public String getSheetName();

    /**
     * moves to next row (next row, 1st column)
     *
     * @throws ExcelPOIWrapperException if column mode is set
     */
    public void nextRow() throws ExcelPOIWrapperException;

    /**
     * Checks if next row exists or not. It compares the current row number with
     * number of rows (minus 1) in the current sheet
     *
     * @return true if next row exists else false
     * @throws ExcelPOIWrapperException if column mode is set
     */
    public Boolean hasNextRow() throws ExcelPOIWrapperException;

    /**
     * moves to next column (next column,1st row)
     *
     * @throws ExcelPOIWrapperException if row mode is set
     */
    public void nextColumn() throws ExcelPOIWrapperException;

    /**
     * Checks if next column exists (assumes that if 1st row of next column
     * exists then whole column exists else no)
     *
     * @return Boolean value
     * @throws ExcelPOIWrapperException if row mode is set
     */
    public Boolean hasNextColumn() throws ExcelPOIWrapperException;

    /**
     * moves to next cell. Depending upon reading mode (row or column) it moves
     * right or downwards By default while reading a cell (calling
     * get*CellValue() functions), it moves to next cell. So this function is
     * used only if manually user needs to skip current cell
     */
    public void nextCell();

    /**
     * @return True if cell is empty. If it has some value, it returns false
     */
    public Boolean isCellEmpty();
    /**
     * @return true if cell exists else false. A cell does not exists if its row
     * is null or the cell is null.
     */
    public Boolean hasCell();

    /**
     * Sets the current row to the given input
     *
     * @param rowNum row number to which we need to move
     */
    public void setRow(int rowNum);

    /**
     * Sets the current column to the given input
     *
     * @param colNum col number to which we need to move
     */
    public void setColumn(int colNum);

    /**
     * Sets the reading mode of the sheet to column wise
     */
    public void setColumnMode();

    /**
     * Sets the reading mode of the sheet to row wise
     */
    public void setRowMode();

    /**
     * Set the current cell to a named reference and assign that reference to
     * string name
     *
     * @param name
     */
    public void setNamedCell(String name);

    /**
     * moves to the cell referenced by input reference string. Now all
     * operations will happen on referenced cell(previous cell location is lost)
     *
     * @param reference reference string (basically reference to the cell)
     * @throws ExcelPOIWrapperException
     */
    public void moveToNamedCell(String reference) throws ExcelPOIWrapperException;

    /**
     * Checks if the given sheet is hidden or not. 
     * If sheet is hidden(can be unhidden by user) or very hidden (cannot be unhidden by user), 
     * in both cases it will return true
     * @return true if sheet is hidden or false if not
     */
    public Boolean isHidden();
            
            
    /**
     * @return returns the type of cell represented by the class CELLTYPE
     */
    public CellType getCellType();

    /**
     *
     * @param readAndMoveParams variable argument(varargs: present in java
     * >1.5). 1st argument denotes if move to next cell after reading if no
     * arguments are provided, by default it moves to next cell after reading
     * @return value of the current cell
     * @throws ExcelPOIWrapperException
     */
    public Object readCellValue(Boolean... readAndMoveParams) throws ExcelPOIWrapperException;

    /**
     * @param readAndMoveParams variable argument(varargs: present in java
     * >1.5). 1st argument denotes if move to next cell after reading if no
     * arguments are provided, by default it moves to next cell after reading
     * @return numeric value of the current cell
     * @throws ExcelPOIWrapperException if cell type is not numeric, it throws
     * this exception
     */
    public double readNumericCellValue(Boolean... readAndMoveParams) throws ExcelPOIWrapperException;

    /**
     * @param readAndMoveParams variable argument(varargs: present in java
     * >1.5). 1st argument denotes if move to next cell after reading if no
     * arguments are provided, by default it moves to next cell after reading
     * @return string value of the current cell. If cell does not exists or
     * empty, an empty string is returned ("")
     * @throws ExcelPOIWrapperException if cell type is not string, it throws
     * this exception
     */
    public String readStringCellValue(Boolean... readAndMoveParams) throws ExcelPOIWrapperException;

    /**
     * @param readAndMoveParams variable argument(varargs: present in java
     * >1.5). 1st argument denotes if move to next cell after reading if no
     * arguments are provided, by default it moves to next cell after reading
     * @return Date value of the current cell
     * @throws ExcelPOIWrapperException if cell type is not Date, it throws this
     * exception
     */
    public Date readDateCellValue(Boolean... readAndMoveParams) throws ExcelPOIWrapperException;

    /**
     * @param readAndMoveParams variable argument(varargs: present in java
     * >1.5). 1st argument denotes if move to next cell after reading if no
     * arguments are provided, by default it moves to next cell after reading
     * @return boolean value of the current cell
     * @throws ExcelPOIWrapperException if cell type is not boolean, it throws
     * this exception
     */
    public Boolean readBoolCellValue(Boolean... readAndMoveParams) throws ExcelPOIWrapperException;
}

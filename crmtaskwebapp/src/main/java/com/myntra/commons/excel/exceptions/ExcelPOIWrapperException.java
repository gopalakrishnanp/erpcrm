package com.myntra.commons.excel.exceptions;

public class ExcelPOIWrapperException extends Exception{
	public ExcelPOIWrapperException(){
	}
	
	public ExcelPOIWrapperException(String msg){
		super(msg);
	}
}

package com.myntra.commons.excel;

import com.myntra.commons.excel.exceptions.ExcelPOIWrapperException;
import com.myntra.commons.excel.poiImpl.MyntraSheetReader;
import java.util.List;

/**
 * @author paroksh
 *
 */
public interface MyntraExcelReaderInterface {

    /**
     * @author paroksh
     * @return List containing object of type MyntraSheetReader which itself
     * will contain sheets read from the input file specified by user in the
     * constructor
     * @throws ExcelPOIWrapperException
     */
    public List<MyntraSheetReader> getSheetReaders() throws ExcelPOIWrapperException;
}

function clickRowIcon(that) {
    if(!$(that).closest('tr').find('td:eq(1) div').hasClass('ui-state-active')){
        console.log(that);
        $(that).closest('tr').find('td:eq(1) div.ui-chkbox-box').trigger('click');
    }
}

$(document).ready(function() {
    // This is to enable opening panel on clicking header
    $('.ui-widget').on('mouseup', 'div.ui-panel-titlebar', function(event) {
        event.stopPropagation();
        // added both class to make it compatible for FF and chrome
        // dont toggle on click of refresh icon on panel header bar
        if (!$(event.target).hasClass("ui-icon-refresh") && !$(event.target).hasClass("ui-button")) {
            if (!$(event.target).is("a.ui-panel-titlebar-icon > span")) {
                if (!$(event.target).is('a.ui-panel-titlebar-icon')) {
                    $(this).find('a .ui-icon-minusthick, a .ui-icon-plusthick').trigger('click');
                }
            }
        }

    });

    $('.ui-widget-content').on('click', 'tr', function(event) {
        
        if ($(event.target).is(".ui-row-toggler")
        || $(event.target).is(".ui-chkbox-box") 
        || $(event.target).is(".ui-chkbox-icon")) {
            return;
        }
        
        if($(event.target).is(".ui-selectonemenu")
        || $(event.target).is(".ui-selectonemenu-trigger")
        || $(event.target).is(".ui-selectonemenu-label")) {
            console.log('select menu');
            return;
        }
        
        var obj = $(this).find('td:eq(0) div');

        if (obj.hasClass('ui-row-toggler')) {
            obj.trigger("click");
            event.stopPropagation();
        }
    });   
});

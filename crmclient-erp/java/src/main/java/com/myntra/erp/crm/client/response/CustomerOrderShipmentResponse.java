package com.myntra.erp.crm.client.response;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.myntra.commons.response.AbstractResponse;
import com.myntra.erp.crm.client.entry.CustomerOrderShipmentEntry;

/**
 * Response for customer order search web service with detail of order shipments
 * 
 * @author Arun Kumar
 */
@XmlRootElement(name = "CustomerOrderShipmentResponse")
public class CustomerOrderShipmentResponse extends AbstractResponse {

	private static final long serialVersionUID = 1L;	
	private List<CustomerOrderShipmentEntry> customerOrderShipmentEntryList;
	
	@XmlElementWrapper(name = "customerOrderShipmentEntryList")
	@XmlElement(name = "customerOrderShipmentEntry")
	public List<CustomerOrderShipmentEntry> getCustomerOrderShipmentEntryList() {
		return customerOrderShipmentEntryList;
	}

	public void setCustomerOrderShipmentEntryList(List<CustomerOrderShipmentEntry> customerOrderShipmentEntryList) {
		this.customerOrderShipmentEntryList = customerOrderShipmentEntryList;
	}

	public CustomerOrderShipmentResponse() {
	}

	@Override
	public String toString() {
		return "CustomerOrderShipmentResponse [customerOrderShipmentEntryList=" + customerOrderShipmentEntryList + "]";
	}
}
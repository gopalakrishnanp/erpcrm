package com.myntra.erp.crm.client;

import com.myntra.commons.client.BaseWebClient;
import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.test.TestHelper;
import com.myntra.commons.utils.ResponseHandler;
import com.myntra.erp.crm.client.response.TripDeliveryStaffResponse;
import com.myntra.erp.crm.constant.CRMErpClientConstants;

/**
 * Web client for trip delivery staff detail
 * 
 * @author Arun Kumar
 */
public class TripDeliveryStaffClient extends ResponseHandler {

	public static final String SERVICE_PREFIX = "/crm/deliveryStaff/";

	public static TripDeliveryStaffResponse getTripDeliveryStaffDetail(String serviceURL, Long staffId,
			boolean isDCDetailNeeded) throws ERPServiceException {

		BaseWebClient client = new BaseWebClient(serviceURL, CRMErpClientConstants.CRM_ERP_URL, TestHelper.dummyContextInfo1);
		client.path(SERVICE_PREFIX + "{id}", staffId);
		client.query("isDCDetailNeeded", isDCDetailNeeded);

		return client.get(TripDeliveryStaffResponse.class);
	}

	public static void main(String args[]) throws ERPServiceException {
		TripDeliveryStaffResponse test = getTripDeliveryStaffDetail("http://localhost:7090/crmservice-erp", 42L,
				true);
		System.out.println(test);
	}

}
package com.myntra.erp.crm.client.code;

import com.myntra.commons.codes.ERPSuccessCodes;
import com.myntra.commons.codes.StatusCodes;

/**
 * List of success codes
 * 
 * @author Arun Kumar
 * 
 */
public class CRMErpSuccessCodes extends ERPSuccessCodes {

	public static final StatusCodes CUSTOMER_ORDER_RETRIEVED = new CRMErpSuccessCodes(701, "CUSTOMER_ORDER_RETRIEVED");
	public static final StatusCodes CUSTOMER_ORDER_SUMMARY_RETRIEVED = new CRMErpSuccessCodes(702,
			"CUSTOMER_ORDER_SUMMARY_RETRIEVED");
	public static final StatusCodes CUSTOMER_RETURN_RETRIEVED = new CRMErpSuccessCodes(703, "CUSTOMER_RETURN_RETRIEVED");		
	public static final StatusCodes CASHBACK_LOG_RETRIEVED = new CRMErpSuccessCodes(705, "CASHBACK_LOG_RETRIEVED");
	public static final StatusCodes CUSTOMER_PROFILE_RETRIEVED = new CRMErpSuccessCodes(706, "CUSTOMER_PROFILE_RETRIEVED");
	public static final StatusCodes TRIP_DELIVERY_STAFF_DETAIL_RETRIEVED = new CRMErpSuccessCodes(707, "TRIP_DELIVERY_STAFF_DETAIL_RETRIEVED");
	
	private static final String BUNDLE_NAME = "CRMErpSuccessCodes";

	public CRMErpSuccessCodes(int successCode, String successMessage) {
		setAll(successCode, successMessage, BUNDLE_NAME);
	}

}

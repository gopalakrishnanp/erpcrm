package com.myntra.erp.crm.client.response;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.myntra.commons.response.AbstractResponse;
import com.myntra.erp.crm.client.entry.ShipmentStatusRevenueEntry;

/**
 * @author Pravin Mehta
 */
@XmlRootElement(name = "customerOrderSummaryResponse")
public class CustomerOrderSummaryResponse extends AbstractResponse {

	private static final long serialVersionUID = 1L;
	private ShipmentStatusRevenueEntry shipmentStatusRevenueEntry;

	@XmlElement(name = "shipmentStatusRevenueEntry")
	public ShipmentStatusRevenueEntry getShipmentStatusRevenueEntry() {
		return shipmentStatusRevenueEntry;
	}

	public void setShipmentStatusRevenueEntry(ShipmentStatusRevenueEntry shipmentStatusRevenueEntry) {
		this.shipmentStatusRevenueEntry = shipmentStatusRevenueEntry;
	}

	public CustomerOrderSummaryResponse() {
		super();
	}

	@Override
	public String toString() {
		return "CustomerOrderSummaryResponse [shipmentStatusRevenueEntry=" + shipmentStatusRevenueEntry + "]";
	}
}
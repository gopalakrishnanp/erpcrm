package com.myntra.erp.crm.client.response;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.myntra.commons.response.AbstractResponse;
import com.myntra.erp.crm.client.entry.CustomerOrderEntry;

/**
 * Response for customer order search web service with detail of shipments,
 * skus, warehouse, tracking, trip detail etc..
 * 
 * @author Arun Kumar
 */
@XmlRootElement(name = "customerOrderResponse")
public class CustomerOrderResponse extends AbstractResponse {

	private static final long serialVersionUID = 1L;
	private List<CustomerOrderEntry> customerOrderEntryList;
	
	@XmlElementWrapper(name = "customerOrderEntryList")
	@XmlElement(name = "customerOrderEntry")
	public List<CustomerOrderEntry> getCustomerOrderEntryList() {
		return customerOrderEntryList;
	}

	public void setCustomerOrderEntryList(List<CustomerOrderEntry> customerOrderEntryList) {
		this.customerOrderEntryList = customerOrderEntryList;
	}

	public CustomerOrderResponse() {
	}

	@Override
	public String toString() {
		return "CustomerOrderResponse [customerOrderEntryList=" + customerOrderEntryList + "]";
	}
}

package com.myntra.erp.crm.oneview.model;

import com.myntra.erp.crm.client.entry.CashbackBalanceEntry;
import com.myntra.erp.crm.client.entry.CashbackLogEntry;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author pravin
 */
public class CashbackModel implements Serializable {
    
    private CashbackBalanceEntry cashbackBalanceEntry;
    private List<CashbackLogEntry> cashbackLogEntryList;

    public CashbackBalanceEntry getCashbackBalanceEntry() {
        return cashbackBalanceEntry;
    }

    public void setCashbackBalanceEntry(CashbackBalanceEntry cashbackBalanceEntry) {
        this.cashbackBalanceEntry = cashbackBalanceEntry;
    }   

    public List<CashbackLogEntry> getCashbackLogEntryList() {
        return cashbackLogEntryList;
    }
    
    public void setCashbackLogEntryList(List<CashbackLogEntry> cashbackLogEntryList) {
        this.cashbackLogEntryList = cashbackLogEntryList;
        
    }
}

package com.myntra.erp.crm.oneview.model;

import com.myntra.commons.client.BaseWebClient;
import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.utils.Context;
import com.myntra.commons.utils.ServiceURLProperty;
import com.myntra.erp.crm.client.CustomerReturnClient;
import com.myntra.erp.crm.client.TripDeliveryStaffClient;
import com.myntra.erp.crm.client.entry.CustomerOrderTripAssignmentEntry;
import com.myntra.erp.crm.client.entry.CustomerReturnEntry;
import com.myntra.erp.crm.client.entry.TripDeliveryStaffEntry;
import com.myntra.erp.crm.client.response.CustomerReturnResponse;
import com.myntra.erp.crm.client.response.TripDeliveryStaffResponse;
import com.myntra.erp.crm.oneview.utils.Utility;
import com.myntra.lms.client.response.OrderResponse;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import org.primefaces.context.RequestContext;

/**
 *
 * @author pravin
 */
public class ReturnModel implements Serializable {

    private List<CustomerReturnEntry> returnEntryList;
    private HashMap<Long, TripDeliveryStaffEntry> shipmentToStaffEntryMap;
    private Long returnIDtoUpdate;

    public List<CustomerReturnEntry> getReturnEntryList() {
        return returnEntryList;
    }

    public void setReturnEntryList(List<CustomerReturnEntry> returnEntryList) {
        this.returnEntryList = returnEntryList;
        Utility.fixReturnComment(this.returnEntryList);
        doReturnEnhancements();
    }
    
    public TripDeliveryStaffEntry getStaffDetail(Long shipmentId) {
        return shipmentToStaffEntryMap.get(shipmentId);
    }
    
    public void doReturnEnhancements(){
        getDeliveryStaffDetail();
    }

    public void fetchData(Long retId, String loginID, OrderModel orderModel) throws ERPServiceException {        
        CustomerReturnResponse response = null;
        if (retId != null) {            
            response = CustomerReturnClient.getReturnById(null, retId);
        } else if (orderModel != null) {
            response = CustomerReturnClient.getReturnByShipmentIdList(null, orderModel.getShipmentIdList());
        } else if (loginID != null) {
            response = CustomerReturnClient.getReturnByLogin(null, loginID);
        }
        if (response != null && response.getStatus().getStatusType().equals("SUCCESS")) {
            this.setReturnEntryList(response.getCustomerReturnEntryList());
        }
    }

    public void markReturn(Long returnId) {        
        returnIDtoUpdate = returnId;
    }

    public void updateMarkedReturn() throws ERPServiceException {
        if (returnIDtoUpdate == null || returnIDtoUpdate == 0) {
            return;
        }

        CustomerReturnResponse response = CustomerReturnClient.getReturnById(null, returnIDtoUpdate);
        CustomerReturnEntry changedEntry = response.getCustomerReturnEntryList().get(0);
        Utility.fixReturnComment(changedEntry);

        for (int i = 0; i < returnEntryList.size(); i++) {
            CustomerReturnEntry entry = returnEntryList.get(i);
            if (entry.getReturnId().equals(returnIDtoUpdate)) {
                returnEntryList.set(i, changedEntry);
                break;
            }
        }        
        // Return ID details fetched.. Reset returnIDtoUpdate to null.
        returnIDtoUpdate = null;
        
        RequestContext context = RequestContext.getCurrentInstance();
        context.update("mainForm:return_data_table");
    }
    
    public void getDeliveryStaffDetail(){
        
        if (shipmentToStaffEntryMap == null) {
            shipmentToStaffEntryMap = new HashMap<Long, TripDeliveryStaffEntry>();
        }
    
        for (CustomerReturnEntry returnEntry : returnEntryList) {
            Long shipmentID = returnEntry.getOrderId();
            List<CustomerOrderTripAssignmentEntry> tripAssignmentEntryList = returnEntry.getTripAssignmentEntry();

            if (tripAssignmentEntryList != null && tripAssignmentEntryList.size() > 0) {

                Long deliveryStaffId = tripAssignmentEntryList.get(0).getDeliveryStaffId();

                try {
                    if (deliveryStaffId != null) {
                        TripDeliveryStaffResponse response = TripDeliveryStaffClient.getTripDeliveryStaffDetail(null, deliveryStaffId, true);
                        if (response != null && response.getStatus().getStatusType().equals("SUCCESS")) {
                            shipmentToStaffEntryMap.put(shipmentID, response.getTripDeliveryStaffEntry());

                        }
                    }
                } catch (ERPServiceException ex) {
                }
            }
        }
    }
    
    public OrderResponse changePickupStatus(String status, long returnId) throws ERPServiceException{
        // TODO move this to LMS Client codebase.
        BaseWebClient client;
        OrderResponse orderResponse = null;
        
        if(status.equalsIgnoreCase("approve")){
            client = new BaseWebClient(null, ServiceURLProperty.LMS_URL, Context.getContextInfo());
            client.path("/order/approveOrReject/{returnId}/APPROVED", returnId);
            orderResponse = client.post(null, OrderResponse.class);
            
        }else if(status.equalsIgnoreCase("reject")){
            client = new BaseWebClient(null, ServiceURLProperty.LMS_URL, Context.getContextInfo());
            client.path("/order/approveOrReject/{returnId}/REJECTED", returnId);
            orderResponse = client.post(null, OrderResponse.class);
            
        }else{
            
        }
        
        return orderResponse;
    }
}
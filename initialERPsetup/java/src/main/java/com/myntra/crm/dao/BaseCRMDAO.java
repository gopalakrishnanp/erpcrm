/**
 * 
 */
package com.myntra.crm.dao;

import javax.persistence.EntityManager;



/**
 * @author	pravin 
 *
 */
public interface BaseCRMDAO<T> {
	
	/**
	 * For read and write operations, the method will be called with true and
	 * false respectively as input parameter and Context.getReadWriteInfo().useSlave(). 
	 * To avoid changing the old getEntityManager method in all services, the current
	 * getEntityManager method will simply call this method with false input.
	 * 
	 * Below I am explaining different scenarios considering slave db is
	 * configured and is turned on:
	 * <ul>
	 * <li>Context.getReadWriteInfo().setApiUseRead(true) is set at service
	 * level and you want to read from slave only. As readOnly = true as well as
	 * ApiUseRead is also true, slave entity manager will be returned.</li>
	 * <li>Context.getReadWriteInfo().setApiUseRead(true) is set at service
	 * level and you want to write. As readOnly = false, master entity manager
	 * will be returned.</li>
	 * <li>Context.getReadWriteInfo().setApiUseRead(true) is not set at service
	 * level and you want to read from slave only. readOnly = true and
	 * ApiUseRead is false. Master entity manager will be returned.</li>
	 * <li>Context.getReadWriteInfo().setApiUseRead(true) is not set at service
	 * level and you want to write. As readOnly = false, master entity manager
	 * will be returned.</li>
	 * <li>Now suppose it's read only operation and ApiUseRead is also true, but
	 * still we want to read from Master, simply call the method will false
	 * input.</li>
	 * <b>NOTE: Slave entity manager will be returned only when 
	 * readOnly parameter is true and Context.getReadWriteInfo().setApiUseRead(true)
	 * is called at service level.</b>
	 * @param readOnly
	 * @return entity manager
	 */
	public EntityManager getEntityManager(boolean readOnly);
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myntra.crm.entity;

import com.myntra.commons.entities.BaseEntity;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Table;
import javax.persistence.Temporal;

/**
 *
 * @author pravin
 */
@Entity
@Table(name = "xcart_customers")
@NamedNativeQueries({
    @NamedNativeQuery(
        name = "CustomerProfileEntity.q1",
        query = "SELECT login, gender, DOB, firstname, lastname, email, phone, mobile, first_login from xcart_customers where login=:login and usertype='C'",
        resultSetMapping = "CustomerProfileEntity.q1.results"
    )
})

@SqlResultSetMappings({
    @SqlResultSetMapping(name="CustomerProfileEntity.q1.results",
        columns = {@ColumnResult(name="login"),
            @ColumnResult(name="gender"),
            @ColumnResult(name="DOB"),
            @ColumnResult(name="firstname"),
            @ColumnResult(name="lastname"),
            @ColumnResult(name="email"),
            @ColumnResult(name="phone"),
            @ColumnResult(name="mobile"),
            @ColumnResult(name="first_login")
        })
})
public class CustomerProfileEntity extends BaseEntity {

    private static final long serialVersionUID = 3688398701763484215L;
    
    @Id
    @Column(name = "login")
    private String login;
        
    @Column(name = "gender")
    private Character gender;
    
    @Column(name = "DOB")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dob;
    
    @Column(name = "firstname")
    private String firstName;
    
    @Column(name = "lastname")
    private String lastName;
    
    @Column(name = "email")
    private String email;
    
    @Column(name = "phone")
    private String phone;
    
    @Column(name = "mobile")
    private String mobile;
        
    @Column(name = "first_login")
    private Integer firstLogin;
    
    public CustomerProfileEntity() {
    }

    
    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public Integer getFirstLogin() {
        return firstLogin;
    }

    public void setFirstLogin(Integer firstLogin) {
        this.firstLogin = firstLogin;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public Character getGender() {
        return gender;
    }

    public void setGender(Character gender) {
        this.gender = gender;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public CustomerProfileEntity(String login, Character gender, Date dob, String firstName, String lastName, String email, String phone, String mobile, Integer firstLogin) {
        this.login = login;
        this.gender = gender;
        this.dob = dob;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.phone = phone;
        this.mobile = mobile;
        this.firstLogin = firstLogin;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("CustomerProfileEntity [")
                .append("login=").append(login)
                .append(", gender=").append(gender)
                .append(", dob=").append(dob)
                .append(", firstName=").append(firstName)
                .append(", lastName=").append(lastName)
                .append(", email=").append(email)
                .append(", phone=").append(phone)
                .append(", mobile=").append(mobile)
                .append(", firstLogin=").append(firstLogin)
                .append("]");
        return builder.toString();
    }
}

package com.myntra.crm.manager.impl;

import com.myntra.commons.manager.BaseManager;
import com.myntra.commons.tranformer.TransformerUtil;
import com.myntra.crm.entity.BaseCRMEntity;
import com.myntra.crm.entry.BaseCRMEntry;
import org.apache.log4j.Logger;

public abstract class BaseCRMManagerImpl<R, E> implements BaseManager<R, E> {

    public BaseCRMEntity getEntityFromEntry(BaseCRMEntry entry, Class entityClass) {
        if (entry != null) {
            try {
                BaseCRMEntity entity = (BaseCRMEntity) TransformerUtil.transformObject(entry, entityClass);
                return entity;
            } catch (Exception e) {
                Logger.getLogger(this.getClass()).error(e);
            }
        }
        return null;
    }

    public BaseCRMEntry getEntryFromEntity(BaseCRMEntity entity, Class entryClass) {
        if (entity != null) {
            try {
                BaseCRMEntry entry = (BaseCRMEntry) TransformerUtil.transformObject(entity, entryClass);
                return entry;
            } catch (Exception e) {
                Logger.getLogger(this.getClass()).error(e);
            }
        }
        return null;
    }

    /**
     * Copies data from entry to entity.
     *
     * @param <T>
     * @param entity. Must not be NULL
     * @param entry. Must not be NULL
     * @return resultant entry
     */
    @Deprecated
    public static <T extends BaseCRMEntry> T toEntry(BaseCRMEntity entity, T entry) {
        if (entity == null || entry == null) {
            return null;
        } else {
            return entry;
        }
    }

    /**
     * Copies data from entity to entry.
     *
     * @param <T>
     * @param entry. Must not be NULL
     * @param entity. Must not be NULL
     * @return resultant entity
     */
    @Deprecated
    public static <T extends BaseCRMEntity> T toEntity(BaseCRMEntry entry, T entity) {
        if (entry == null || entity == null) {
            return null;
        } else {
            return entity;
        }
    }
}

package com.myntra.crm.manager;

import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.manager.BaseManager;
import com.myntra.crm.client.response.CustomerOrderResponse;
import com.myntra.crm.entry.CustomerOrderEntry;

/**
 * Manager interface(abstract) for customer order search web service which integrates detail of order, shipments, skus,
 * warehouse, tracking, trip detail etc..
 * 
 * @author Arun Kumar
 */
public interface CustomerOrderManager extends
		BaseManager<CustomerOrderResponse, CustomerOrderEntry> {

	public CustomerOrderResponse getOrderSummary(int start, int fetchSize,
			String sortBy, String sortOrder, String searchTerms)
			throws ERPServiceException;
}
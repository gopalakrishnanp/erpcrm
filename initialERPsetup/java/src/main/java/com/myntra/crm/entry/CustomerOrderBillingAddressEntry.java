package com.myntra.crm.entry;

import javax.xml.bind.annotation.XmlRootElement;

import com.myntra.commons.entries.BaseEntry;

/**
 * Entry for customer order response with relevant billing address fields
 * 
 * @author Arun Kumar
 */
@XmlRootElement(name = "order")
@SuppressWarnings("serial")
public class CustomerOrderBillingAddressEntry extends BaseEntry {

	private String billingFirstName;
	private String billingLastName;
	private String billingAddress;
	private String billingCity;
	private String billingCounty;
	private String billingState;
	private String billingZipCode;
	private String billingMobile;
	private String billingEmail;

	public CustomerOrderBillingAddressEntry() {

	}

	public CustomerOrderBillingAddressEntry(String billingFirstName,
			String billingLastName, String billingAddress, String billingCity,
			String billingCounty, String billingState, String billingZipCode,
			String billingMobile, String billingEmail) {
		this.billingFirstName = billingFirstName;
		this.billingLastName = billingLastName;
		this.billingAddress = billingAddress;
		this.billingCity = billingCity;
		this.billingCounty = billingCounty;
		this.billingState = billingState;
		this.billingZipCode = billingZipCode;
		this.billingMobile = billingMobile;
		this.billingEmail = billingEmail;
	}

	public String getBillingFirstName() {
		return billingFirstName;
	}

	public void setBillingFirstName(String billingFirstName) {
		this.billingFirstName = billingFirstName;
	}

	public String getBillingLastName() {
		return billingLastName;
	}

	public void setBillingLastName(String billingLastName) {
		this.billingLastName = billingLastName;
	}

	public String getBillingAddress() {
		return billingAddress;
	}

	public void setBillingAddress(String billingAddress) {
		this.billingAddress = billingAddress;
	}

	public String getBillingCity() {
		return billingCity;
	}

	public void setBillingCity(String billingCity) {
		this.billingCity = billingCity;
	}

	public String getBillingCounty() {
		return billingCounty;
	}

	public void setBillingCounty(String billingCounty) {
		this.billingCounty = billingCounty;
	}

	public String getBillingState() {
		return billingState;
	}

	public void setBillingState(String billingState) {
		this.billingState = billingState;
	}

	public String getBillingZipCode() {
		return billingZipCode;
	}

	public void setBillingZipCode(String billingZipCode) {
		this.billingZipCode = billingZipCode;
	}

	public String getBillingMobile() {
		return billingMobile;
	}

	public void setBillingMobile(String billingMobile) {
		this.billingMobile = billingMobile;
	}

	public String getBillingEmail() {
		return billingEmail;
	}

	public void setBillingEmail(String billingEmail) {
		this.billingEmail = billingEmail;
	}

	@Override
	public String toString() {
		return "BillingAddressEntry [billingFirstName=" + billingFirstName
				+ ", billingLastName=" + billingLastName + ", billingAddress="
				+ billingAddress + ", billingCity=" + billingCity
				+ ", billingCounty=" + billingCounty + ", billingState="
				+ billingState + ", billingZipCode=" + billingZipCode
				+ ", billingMobile=" + billingMobile + ", billingEmail="
				+ billingEmail + "]";
	}
}
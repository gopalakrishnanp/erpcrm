package com.myntra.crm.client.response;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.myntra.commons.response.AbstractResponse;
import com.myntra.crm.entry.CustomerOrderShipmentEntry;

/**
 * Response for customer order search web service with detail of order shipments
 * 
 * @author Arun Kumar
 */
@XmlRootElement(name = "orderShipmentResponse")
public class CustomerOrderShipmentResponse extends AbstractResponse {

	private static final long serialVersionUID = 1L;

	private List<CustomerOrderShipmentEntry> orderShipmentEntry;

	public CustomerOrderShipmentResponse() {
	}

	public CustomerOrderShipmentResponse(
			List<CustomerOrderShipmentEntry> orderShipmentEntry) {
		this.orderShipmentEntry = orderShipmentEntry;
	}

	@XmlElementWrapper(name = "data")
	@XmlElement(name = "orderShipment")
	public List<CustomerOrderShipmentEntry> getData() {
		return orderShipmentEntry;
	}

	public void setData(List<CustomerOrderShipmentEntry> orderShipmentEntry) {
		this.orderShipmentEntry = orderShipmentEntry;
	}

	@Override
	public String toString() {
		return "OrderShipmentResponse [data=" + orderShipmentEntry
				+ ", getStatus()=" + getStatus() + "]";
	}

}
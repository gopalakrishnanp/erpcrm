#!/bin/bash
servicesToBlockFile=/var/www/html/servicesToBlock.php

serviceName='crmservice-erp'

if [ -f $servicesToBlockFile ]
then
    echo "Bringing service - $serviceName - into LB"
    sed -i "s/^'$serviceName'/#'$serviceName'/g" $servicesToBlockFile
fi

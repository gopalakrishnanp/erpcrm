package com.myntra.rightnow.crm.client.code;

import com.myntra.commons.codes.ERPErrorCodes;
import com.myntra.commons.codes.StatusCodes;

/**
 * CRM Rightnow error codes
 * 
 */
public class DrishtiErrorCodes extends ERPErrorCodes {
	
	public static final StatusCodes NO_DETAILS_FOR_DRISHTI = new DrishtiErrorCodes(911, "NO_DETAILS_FOR_DRISHTI");
	public static final StatusCodes WRONG_INPUT_PARAMETERS = new DrishtiErrorCodes(912, "WRONG_INPUT_PARAMETERS");

	private static final String BUNDLE_NAME = "DrishtiErrorCodes";

	public DrishtiErrorCodes(int errorCode, String errorMessage) {
		setAll(errorCode, errorMessage, BUNDLE_NAME);
	}

}

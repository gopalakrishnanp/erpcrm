package com.myntra.rightnow.crm.client.entry;

/**
 * @author preetam
 *
 */
public class RightnowTaskReturnEntry extends RightnowBaseTaskEntry {

	private static final long serialVersionUID = 1L;

	
	private long returnId;


	public long getReturnId() {
		return returnId;
	}


	public void setReturnId(long returnId) {
		this.returnId = returnId;
	}


	@Override
	public String toString() {
		return "RightnowTaskReturnEntry [returnId=" + returnId + "]";
	}
	
	
}

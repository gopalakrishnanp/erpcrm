package com.myntra.drishti.crm.client.response;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.myntra.commons.response.AbstractResponse;
import com.myntra.drishti.crm.client.entry.DrishtiOrderEntry;

@XmlRootElement(name = "ivrTestOrderResponse")
public class DrishtiIvrOrderTestResponse extends AbstractResponse {

	private static final long serialVersionUID = 1L;

	private List<DrishtiOrderEntry> drishtiOrderEntryList;

	@XmlElementWrapper(name = "ivrTestOrders")
	@XmlElement(name = "ivrTestOrder")
	public List<DrishtiOrderEntry> getDrishtiOrderEntryList() {
		return drishtiOrderEntryList;
	}

	public void setDrishtiOrderEntryList(List<DrishtiOrderEntry> drishtiOrderEntryList) {
		this.drishtiOrderEntryList = drishtiOrderEntryList;
	}

	@Override
	public String toString() {
		return "DrishtiIvrOrderTestResponse [drishtiOrderEntryList=" + drishtiOrderEntryList + "]";
	}

}
package com.myntra.erp.crm.service;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.response.AbstractResponse;
import com.myntra.commons.service.BaseService;
import com.myntra.erp.crm.client.entry.TripDeliveryStaffEntry;
import com.myntra.erp.crm.client.response.TripDeliveryStaffResponse;

/**
 * Trip delivery staff service interface to retrieve the detail of the trip
 * delivery staff along with delivery center detail
 * 
 * @author Arun Kumar
 */

@Path("/deliveryStaff/{deliveryStaffId}")
public interface TripDeliveryStaffService extends BaseService<TripDeliveryStaffResponse, TripDeliveryStaffEntry> {

	@GET
	@Produces({ "application/xml", "application/json" })
	@Consumes({ "application/xml", "application/json" })
	AbstractResponse getTripDeliveryStaffDetail(@PathParam("deliveryStaffId") Long deliveryStaffId,
			@DefaultValue("true") @QueryParam("isDCDetailNeeded") boolean isDCDetailNeeded)

	throws ERPServiceException;
}

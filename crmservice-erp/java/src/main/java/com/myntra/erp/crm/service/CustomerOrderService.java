package com.myntra.erp.crm.service;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.response.AbstractResponse;
import com.myntra.commons.service.BaseService;
import com.myntra.erp.crm.client.entry.CustomerOrderEntry;
import com.myntra.erp.crm.client.response.CustomerOrderResponse;

/**
 * Customer order search web service interface(abstract) which integrates detail
 * of order, shipments, skus, warehouse, tracking, trip detail etc..
 * 
 * @author Arun Kumar
 */
@Path("/order/")
public interface CustomerOrderService extends BaseService<CustomerOrderResponse, CustomerOrderEntry> {

	@GET
	@Produces({ "application/xml", "application/json" })
	@Consumes({ "application/xml", "application/json" })
	@Path("/")
	AbstractResponse getOrderDetails(@DefaultValue("0") @QueryParam("start") int start,
			@DefaultValue("-1") @QueryParam("fetchSize") int fetchSize,
			@DefaultValue("NOSORT") @QueryParam("sortBy") String sortBy,
			@DefaultValue("ASC") @QueryParam("sortOrder") String sortOrder, @QueryParam("q") String searchTerms,
			@DefaultValue("true") @QueryParam("isWarehouseDetailNeeded") boolean isWarehouseDetailNeeded,
			@DefaultValue("true") @QueryParam("isSKUDetailNeeded") boolean isSKUDetailNeeded,
			@DefaultValue("true") @QueryParam("isLogisticDetailNeeded") boolean isLogisticDetailNeeded,
			@DefaultValue("true") @QueryParam("isPaymentLogNeeded") boolean isPaymentLogNeeded)	
	throws ERPServiceException;
	
	@GET
	@Produces({ "application/xml", "application/json" })
	@Consumes({ "application/xml", "application/json" })
	@Path("/minimum/")
	AbstractResponse getOrderMinimumDetails(@DefaultValue("0") @QueryParam("start") int start,
			@DefaultValue("-1") @QueryParam("fetchSize") int fetchSize,
			@DefaultValue("NOSORT") @QueryParam("sortBy") String sortBy,
			@DefaultValue("ASC") @QueryParam("sortOrder") String sortOrder, @QueryParam("q") String searchTerms
			)
	throws ERPServiceException;
	
}



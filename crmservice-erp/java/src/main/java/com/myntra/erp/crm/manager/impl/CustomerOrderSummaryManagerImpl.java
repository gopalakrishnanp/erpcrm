package com.myntra.erp.crm.manager.impl;

import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;

import com.myntra.commons.codes.StatusResponse;
import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.manager.impl.BaseManagerImpl;
import com.myntra.commons.response.AbstractResponse;
import com.myntra.erp.crm.client.code.CRMErpSuccessCodes;
import com.myntra.erp.crm.client.entry.CustomerOrderEntry;
import com.myntra.erp.crm.client.entry.CustomerOrderItemEntry;
import com.myntra.erp.crm.client.entry.CustomerOrderShipmentEntry;
import com.myntra.erp.crm.client.entry.CustomerOrderSummaryEntry;
import com.myntra.erp.crm.client.entry.ShipmentStatusRevenueEntry;
import com.myntra.erp.crm.client.response.CustomerOrderResponse;
import com.myntra.erp.crm.client.response.CustomerOrderSummaryResponse;
import com.myntra.erp.crm.manager.CustomerOrderManager;
import com.myntra.erp.crm.manager.CustomerOrderSummaryManager;
import com.myntra.portal.crm.client.code.CRMPortalSuccessCodes;

/**
 * @author Pravin Mehta
 */
public class CustomerOrderSummaryManagerImpl extends
		BaseManagerImpl<CustomerOrderSummaryResponse, CustomerOrderSummaryEntry> implements CustomerOrderSummaryManager {

	private static final Logger LOGGER = Logger.getLogger(CustomerOrderSummaryManagerImpl.class);

	private CustomerOrderManager orderManager;

	public CustomerOrderManager getOrderManager() {
		return orderManager;
	}

	public void setOrderManager(CustomerOrderManager orderManager) {
		this.orderManager = orderManager;
	}

	@Override
	public CustomerOrderSummaryResponse getOrderSummary(String login) throws ERPServiceException {

		CustomerOrderSummaryResponse response = new CustomerOrderSummaryResponse();
		HashMap<String, Double> orderStatusMap = new HashMap<String, Double>();
		HashMap<String, Integer> quantityStatusMap = new HashMap<String, Integer>();

		try {

			CustomerOrderResponse orderDetailsReponse = getOrderManager().getOrderDetails(0, -1, null, null,
					"login.eq:" + login, false, false, false, false);

			List<CustomerOrderEntry> customerOrderEntryList = orderDetailsReponse.getCustomerOrderEntryList();
			

			for (CustomerOrderEntry customerOrderEntry : customerOrderEntryList) {
				List<CustomerOrderShipmentEntry> customerOrderShipmentList = customerOrderEntry.getOrderShipments();

				for (CustomerOrderShipmentEntry customerOrderShipment : customerOrderShipmentList) {
					List<CustomerOrderItemEntry> orderItemList = customerOrderShipment.getOrderItems();

					for (CustomerOrderItemEntry orderItem : orderItemList) {
						String status = customerOrderShipment.getStatus(); // Order
																			// Status..
																			// (Not
																			// to
																			// confuse
																			// with
																			// item
																			// status)

						Double cashbackRedeemed = orderItem.getCashRedeemed();
						Double finalAmount = orderItem.getFinalAmount();

						Double amountPaid = orderStatusMap.get(status);

						if (amountPaid == null)
							amountPaid = Double.valueOf(0);

						amountPaid = amountPaid + finalAmount + cashbackRedeemed; // For
																					// summary

						Integer quantity = quantityStatusMap.get(status);
						if (quantity == null)
							quantity = 0;

						quantity = quantity + orderItem.getQuantity();

						orderStatusMap.put(status, amountPaid);
						quantityStatusMap.put(status, quantity);
					}
				}
			}

			ShipmentStatusRevenueEntry statusRevenue = new ShipmentStatusRevenueEntry();
			statusRevenue.setShipmentStatusRevenueMap(orderStatusMap);
			statusRevenue.setShipmentStatusQuantityMap(quantityStatusMap);

			response.setShipmentStatusRevenueEntry(statusRevenue);

		} catch (ERPServiceException e) {
			LOGGER.error("Error validating/retreiving data", e);
			StatusResponse error = new StatusResponse(e.getCode(), e.getMessage(), StatusResponse.Type.ERROR);
			response.setStatus(error);
			throw new ERPServiceException(response);
		} catch (Exception e) {
			LOGGER.error("error", e);
			StatusResponse error = new StatusResponse(1, e.getMessage(), StatusResponse.Type.ERROR);
			response.setStatus(error);
			throw new ERPServiceException(response);
		}

		if (orderStatusMap.size() <= 0 && quantityStatusMap.size() <= 0) {
			StatusResponse success = new StatusResponse(CRMPortalSuccessCodes.CUSTOMER_PROFILE_RETRIEVED,
					StatusResponse.Type.SUCCESS, 0);			
			response.setStatus(success);	
			
		} else {
			StatusResponse success = new StatusResponse(CRMErpSuccessCodes.CUSTOMER_ORDER_SUMMARY_RETRIEVED,
					StatusResponse.Type.SUCCESS, 1);
			response.setStatus(success);
		}		

		return response;
	}

	@Override
	public AbstractResponse findById(Long id) throws ERPServiceException {
		throw new UnsupportedOperationException("Not supported yet.");

	}

	@Override
	public AbstractResponse create(CustomerOrderSummaryEntry entry) throws ERPServiceException {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public AbstractResponse update(CustomerOrderSummaryEntry entry, Long itemId) throws ERPServiceException {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public AbstractResponse search(int start, int fetchSize, String sortBy, String sortOrder, String searchTerms)
			throws ERPServiceException {
		throw new UnsupportedOperationException("Not supported yet.");
	}

}

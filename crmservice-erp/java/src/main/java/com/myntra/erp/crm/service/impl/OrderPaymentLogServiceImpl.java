package com.myntra.erp.crm.service.impl;


import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.response.AbstractResponse;
import com.myntra.commons.service.impl.BaseServiceImpl;
import com.myntra.erp.crm.manager.OrderPaymentLogManager;
import com.myntra.erp.crm.service.OrderPaymentLogService;
import com.myntra.portal.crm.client.entry.OrderPaymentLogEntry;
import com.myntra.portal.crm.client.response.OrderPaymentLogResponse;

/**
 * Order payment log service interface to retrieve the detail of order payment
 * and gateway
 * 
 * @author Arun Kumar
 */
public class OrderPaymentLogServiceImpl extends BaseServiceImpl<OrderPaymentLogResponse, OrderPaymentLogEntry>
		implements OrderPaymentLogService {

	@Override
	public AbstractResponse getOrderPaymentLog(Long orderId) throws ERPServiceException {
		return ((OrderPaymentLogManager)getManager()).getOrderPaymentLog(orderId);
	}
}

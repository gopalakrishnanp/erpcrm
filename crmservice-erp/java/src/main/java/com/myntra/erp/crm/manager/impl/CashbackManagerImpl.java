/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myntra.erp.crm.manager.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.myntra.commons.codes.StatusResponse;
import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.manager.impl.BaseManagerImpl;
import com.myntra.commons.response.AbstractResponse;
import com.myntra.erp.crm.client.code.CRMErpSuccessCodes;
import com.myntra.erp.crm.client.entry.CashbackBalanceEntry;
import com.myntra.erp.crm.client.entry.CashbackLogEntry;
import com.myntra.erp.crm.client.response.CashbackResponse;
import com.myntra.erp.crm.manager.CashbackManager;
import com.myntra.erp.crm.util.WebserviceUtil;

/**
 * @author Pravin Mehta
 */
public class CashbackManagerImpl extends BaseManagerImpl<CashbackResponse, CashbackLogEntry> implements CashbackManager {

	private static final Logger LOGGER = Logger.getLogger(CashbackManagerImpl.class);

	@Override
	public CashbackResponse getCashbackLogs(String login) throws ERPServiceException {
		CashbackResponse response = new CashbackResponse();

		CashbackBalanceEntry cashbackBalanceEntry = new CashbackBalanceEntry();
		List<CashbackLogEntry> list = new ArrayList<CashbackLogEntry>();

		try {
			// TODO Get rid of this httpClient and use BaseWebClient instead.

			String cashbackURL = WebserviceUtil.getServiceUrlForKey("cashbackURL");
			String transactionURLpath = "transactionlogs/consolidated/";
			String balanceURLpath = "balance/";

			HttpGet getRequest = new HttpGet(cashbackURL + transactionURLpath + login);
			getRequest.addHeader("accept", "application/json");

			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpResponse httpResponse = httpClient.execute(getRequest);

			if (httpResponse.getStatusLine().getStatusCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + httpResponse.getStatusLine().getStatusCode());
			}

			BufferedReader br = new BufferedReader(new InputStreamReader((httpResponse.getEntity().getContent())));

			String data;
			StringBuilder outputBuilder = new StringBuilder(2048);
			while ((data = br.readLine()) != null) {
				outputBuilder.append(data);
			}

			JSONArray jsonArray = new JSONArray(outputBuilder.toString());

			// for (int i = 0; i < jsonArray.length(); i++) {
			for (int i = jsonArray.length() - 1; i >= 0; i--) {
				JSONObject cashbackDetails = jsonArray.getJSONObject(i);
				CashbackLogEntry cashbackEntry = new CashbackLogEntry(cashbackDetails.getLong("cashbackAccountId"),
						cashbackDetails.getLong("itemId"), cashbackDetails.getString("itemType"),
						cashbackDetails.getString("businesProcess"), cashbackDetails.getDouble("creditInflow"),
						cashbackDetails.getDouble("creditOutflow"), cashbackDetails.getDouble("balance"),
						cashbackDetails.getLong("modifiedOn"), cashbackDetails.getString("modifiedBy"),
						cashbackDetails.getString("description"));

				list.add(cashbackEntry);
			}

			// Now sort by modified date.
			/*
			 * Collections.sort(list, new Comparator<CashbackLogEntry>() {
			 * public int compare(CashbackLogEntry one, CashbackLogEntry other)
			 * { if(null!=one && null!=other) { return
			 * other.getModifiedOn().compareTo(one.getModifiedOn()); } return 0;
			 * } });
			 */

			getRequest = new HttpGet(cashbackURL + balanceURLpath + login);
			httpResponse = httpClient.execute(getRequest);

			br = new BufferedReader(new InputStreamReader((httpResponse.getEntity().getContent())));

			String data2;
			StringBuilder outputBuilder2 = new StringBuilder(2048);
			while ((data2 = br.readLine()) != null) {
				outputBuilder2.append(data2);
			}

			JSONObject jsonObject = new JSONObject(outputBuilder2.toString());
			cashbackBalanceEntry.setCashbackBalance(jsonObject.getDouble("balance"));
			cashbackBalanceEntry.setStoreCreditBalance(jsonObject.getDouble("storeCreditBalance"));
			cashbackBalanceEntry.setEarnedCreditBalance(jsonObject.getDouble("earnedCreditbalance"));

			response.setCashbackBalanceEntry(cashbackBalanceEntry);
			response.setCashbackLogEntryList(list);

			LOGGER.debug("CashbackResponse object is " + response.toString());

		} catch (JSONException e) {
			StatusResponse error = new StatusResponse(1, e.getMessage(), StatusResponse.Type.ERROR);
			response.setStatus(error);
			throw new ERPServiceException(response);
		} catch (ClientProtocolException e) {
			StatusResponse error = new StatusResponse(1, e.getMessage(), StatusResponse.Type.ERROR);
			response.setStatus(error);
			throw new ERPServiceException(response);
		} catch (IOException e) {
			StatusResponse error = new StatusResponse(1, e.getMessage(), StatusResponse.Type.ERROR);
			response.setStatus(error);
			throw new ERPServiceException(response);
		}

		// return response with status
		StatusResponse success = new StatusResponse(CRMErpSuccessCodes.CASHBACK_LOG_RETRIEVED,
				StatusResponse.Type.SUCCESS, 1);
		response.setStatus(success);

		return response;

	}

	@Override
	public AbstractResponse findById(Long id) throws ERPServiceException {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public AbstractResponse create(CashbackLogEntry entry) throws ERPServiceException {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public AbstractResponse update(CashbackLogEntry entry, Long itemId) throws ERPServiceException {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public AbstractResponse search(int start, int fetchSize, String sortBy, String sortOrder, String searchTerms)
			throws ERPServiceException {
		throw new UnsupportedOperationException("Not supported yet.");
	}
}

<?php
$url="https://myntra.custhelp.com/cgi-bin/myntra.cfg/php/custom/unassign_incidents_from_log_out_agent.php";

$ch = curl_init();
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible;)");
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_HTTPGET, true);
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
curl_setopt($ch, CURLOPT_TIMEOUT, 600);

$accept = 'Accept: application/json';
$contentType = 'Content-Type: application/json';
curl_setopt($ch, CURLOPT_HTTPHEADER, array("$accept","$contentType"));

$response = curl_exec($ch);
$info = curl_getinfo($ch);

//print_r($response);
//print_r($info);
$currentTime = date("Y-m-d H:i:s");
$response = $currentTime . " :  " . $response . "\n";

$myFile = "/myntra/crm/cronjobs/unassignedIncident.txt";
$fh = fopen($myFile, 'a') or die("can't open file");
fwrite($fh, $response);


// Running this again.. For safe check
//$response = curl_exec($ch);
//$response = $currentTime . " :  " . $response . "\n\n";
//fwrite($fh, $response);

fclose($fh);


?>


package com.myntra.portal.crm.client.entry;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

import com.myntra.commons.entries.BaseEntry;

/**
 * Entry for customer order comments
 * 
 * @author Arun Kumar
 */
@XmlRootElement(name = "customerOrderCommentEntry")
public class CustomerOrderCommentEntry extends BaseEntry {

	private static final long serialVersionUID = 1L;

	private Long commentId;
	private Long orderId;
	private String type;
	private String title;
	private String commentBy;
	private String description;
	private String newAddress;
	private String giftWrapMessage;
	private Date commentDate;
	private String attachmentName;
	
	public CustomerOrderCommentEntry() {

	}
	
	public CustomerOrderCommentEntry(Long commentId, Long orderId, String type, String title, String commentBy,
			String description, String newAddress, String giftWrapMessage, Date commentDate, String attachmentName) {	
		this.commentId = commentId;
		this.orderId = orderId;
		this.type = type;
		this.title = title;
		this.commentBy = commentBy;
		this.description = description;
		this.newAddress = newAddress;
		this.giftWrapMessage = giftWrapMessage;
		this.commentDate = commentDate;
		this.attachmentName = attachmentName;
	}

	public Long getCommentId() {
		return commentId;
	}

	public Long getOrderId() {
		return orderId;
	}

	public String getType() {
		return type;
	}

	public String getTitle() {
		return title;
	}

	public String getCommentBy() {
		return commentBy;
	}

	public String getDescription() {
		return description;
	}

	public String getNewAddress() {
		return newAddress;
	}

	public String getGiftWrapMessage() {
		return giftWrapMessage;
	}

	public Date getCommentDate() {
		return commentDate;
	}

	public void setCommentId(Long commentId) {
		this.commentId = commentId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setCommentBy(String commentBy) {
		this.commentBy = commentBy;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setNewAddress(String newAddress) {
		this.newAddress = newAddress;
	}

	public void setGiftWrapMessage(String giftWrapMessage) {
		this.giftWrapMessage = giftWrapMessage;
	}

	public void setCommentDate(Date commentDate) {
		this.commentDate = commentDate;
	}	
	
	public String getAttachmentName() {
		return attachmentName;
	}

	public void setAttachmentName(String attachmentName) {
		this.attachmentName = attachmentName;
	}

	@Override
	public String toString() {
		return "CustomerOrderCommentEntry [commentId=" + commentId + ", orderId=" + orderId + ", type=" + type
				+ ", title=" + title + ", commentBy=" + commentBy + ", description=" + description + ", newAddress="
				+ newAddress + ", giftWrapMessage=" + giftWrapMessage + ", commentDate=" + commentDate + ", attachmentName " + attachmentName + "]";
	}	
}

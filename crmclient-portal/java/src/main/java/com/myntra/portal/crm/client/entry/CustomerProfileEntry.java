package com.myntra.portal.crm.client.entry;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

import com.myntra.commons.entries.BaseEntry;

/**
 * Person details used outside DAO
 * 
 */
@XmlRootElement(name = "customerProfileEntry")
public class CustomerProfileEntry extends BaseEntry{

	private static final long serialVersionUID = 1L;
	private String login;
	private String gender;
	private Date dob;
	private String firstName;
	private String lastName;
	private String email;
	private String phone;
	private String referer;
	private String mobile;
	private Date firstLogin;
	private Date lastLogin;
	private String status;

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	public String getReferer() {
		return referer;
	}

	public void setReferer(String referer) {
		this.referer = referer;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public Date getFirstLogin() {
		return firstLogin;
	}

	public void setFirstLogin(Date firstLogin) {
		this.firstLogin = firstLogin;
	}
	
	public Date getLastLogin() {
		return lastLogin;
	}

	public void setLastLogin(Date lastLogin) {
		this.lastLogin = lastLogin;
	}
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();

		builder.append("CustomerProfileEntity [").append("login=").append(login)
				.append(", gender=").append(gender)
				.append(", dob=").append(dob)
				.append(", firstName=").append(firstName)
				.append(", lastName=").append(lastName)
				.append(", email=").append(email)
				.append(", phone=").append(phone)
				.append(", referer=").append(referer)
				.append(", mobile=").append(mobile)
				.append(", firstLogin=").append(firstLogin)
				.append(", lastLogin=").append(lastLogin)
				.append(", status=").append(status)
				.append("]");
		return builder.toString();
	}

	public CustomerProfileEntry(String login, String gender, Date dob, String firstName, String lastName, String email,
			String phone, String referer, String mobile, Date firstLogin, Date lastLogin, String status) {
		this.login = login;
		this.gender = gender;
		this.dob = dob;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.phone = phone;
		this.referer = referer;
		this.mobile = mobile;
		this.firstLogin = firstLogin;
		this.lastLogin = lastLogin;
		this.status = status;
	}

	public CustomerProfileEntry() {
		super();
	}
}

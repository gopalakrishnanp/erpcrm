package com.myntra.portal.crm.client.entry;

import javax.xml.bind.annotation.XmlRootElement;

import com.myntra.commons.entries.BaseEntry;

/**
 * Entry for customer order exchange
 * 
 * @author Pravin Mehta
 */
@XmlRootElement(name = "orderExchangeEntry")
public class OrderExchangeEntry extends BaseEntry {

	private static final long serialVersionUID = 1L;

	private Long exchangeOrderId;
	private Long shipmentId;
	private Long itemId;
	private Long returnId;
	private String returnType;

	public OrderExchangeEntry() {
	}
	
	public OrderExchangeEntry(Long exchangeOrderId, Long shipmentId,
			Long itemId, Long returnId, String returnType) {
		this.exchangeOrderId = exchangeOrderId;
		this.shipmentId = shipmentId;
		this.itemId = itemId;
		this.returnId = returnId;
		this.returnType = returnType;
	}

	public Long getExchangeOrderId() {
		return exchangeOrderId;
	}

	public void setExchangeOrderId(Long exchangeOrderId) {
		this.exchangeOrderId = exchangeOrderId;
	}

	public Long getShipmentId() {
		return shipmentId;
	}

	public void setShipmentId(Long shipmentId) {
		this.shipmentId = shipmentId;
	}

	public Long getItemId() {
		return itemId;
	}

	public void setItemId(Long itemId) {
		this.itemId = itemId;
	}

	public Long getReturnId() {
		return returnId;
	}

	public void setReturnId(Long returnId) {
		this.returnId = returnId;
	}

	public String getReturnType() {
		return returnType;
	}

	public void setReturnType(String returnType) {
		this.returnType = returnType;
	}

	@Override
	public String toString() {
		return "OrderExchangeEntry [exchangeOrderId=" + exchangeOrderId
				+ ", shipmentId=" + shipmentId + ", itemId=" + itemId
				+ ", returnId=" + returnId + ", returnType=" + returnType + "]";
	}

}
package com.myntra.portal.crm.client.response;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.myntra.commons.response.AbstractResponse;
import com.myntra.portal.crm.client.entry.OrderRtoEntry;

/**
 * Response for order RTO
 * 
 * @author Arun Kumar
 */
@XmlRootElement(name = "orderRtoResponse")
public class OrderRtoResponse extends AbstractResponse {

	private static final long serialVersionUID = 1L;
	private List<OrderRtoEntry> orderRtoEntryList;

	public OrderRtoResponse() {
	}

	@XmlElement(name = "orderRtoEntryList")
	public List<OrderRtoEntry> getOrderRtoEntryList() {
		return orderRtoEntryList;
	}

	public void setOrderRtoEntryList(List<OrderRtoEntry> orderRtoEntryList) {
		this.orderRtoEntryList = orderRtoEntryList;
	}

	@Override
	public String toString() {
		return "OrderRtoResponse [orderRtoEntryList=" + orderRtoEntryList + "]";
	}

}
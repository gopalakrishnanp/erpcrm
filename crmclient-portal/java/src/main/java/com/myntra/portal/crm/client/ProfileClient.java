package com.myntra.portal.crm.client;

import com.myntra.commons.client.BaseWebClient;
import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.test.TestHelper;
import com.myntra.commons.utils.ResponseHandler;
import com.myntra.portal.crm.client.constant.CRMPortalClientConstants;
import com.myntra.portal.crm.client.response.CustomerProfileResponse;

/**
 * Web client for all the contact web services
 */
public class ProfileClient extends ResponseHandler {

	public static final String SERVICE_PREFIX = "/crm/customerprofile/";

	public static CustomerProfileResponse findByLogin(String serviceURL, String loginID) throws ERPServiceException {

		BaseWebClient client = new BaseWebClient(serviceURL, CRMPortalClientConstants.CRM_PORTAL_URL,
				TestHelper.dummyContextInfo1);
		client.path(SERVICE_PREFIX);
		client.query("login", loginID);
		return client.get(CustomerProfileResponse.class);
	}
	
	public static CustomerProfileResponse findByMobile(String serviceURL, String mobile) throws ERPServiceException {

		BaseWebClient client = new BaseWebClient(serviceURL, CRMPortalClientConstants.CRM_PORTAL_URL,
				TestHelper.dummyContextInfo1);
		client.path(SERVICE_PREFIX + "mobile");
		client.query("mobile", mobile);
		return client.get(CustomerProfileResponse.class);
	}

	public static void main(String args[]) throws ERPServiceException {
		CustomerProfileResponse test = findByLogin(null, "pravin.mehta@myntra.com");
		System.out.println(test);
	}

}

package com.myntra.portal.crm.client.response;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.myntra.commons.response.AbstractResponse;
import com.myntra.portal.crm.client.entry.SREntry;

/**
 * Response for SR web service
 * 
 * @author Arun Kumar
 */
@XmlRootElement(name = "SRResponse")
public class SRResponse extends AbstractResponse {

	private static final long serialVersionUID = 1L;
	private List<SREntry> srEntryList;

	public SRResponse() {
	}

	@XmlElementWrapper(name = "SREntryList")
	@XmlElement(name = "SREntry")
	public List<SREntry> getSREntryList() {
		return srEntryList;
	}

	public void setSREntryList(List<SREntry> srEntryList) {
		this.srEntryList = srEntryList;
	}

	@Override
	public String toString() {
		return "SRResponse [srEntryList=" + srEntryList + "]";
	}
}
package com.myntra.portal.crm.client.entry;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

import com.myntra.commons.entries.BaseEntry;

/**
 * Entry for customer return comments
 * 
 * @author Arun Kumar
 */
@XmlRootElement(name = "returnCommentEntry")
public class ReturnCommentEntry extends BaseEntry {

	private static final long serialVersionUID = 1L;

	private String returnComment;
	private String returnCommentBy;
	private String returnCommentDescription;
	private Date returnCommentCreatedDate;
	private String imageURL;

	public ReturnCommentEntry() {
	}

	public ReturnCommentEntry(String returnComment, String returnCommentBy, String returnCommentDescription,
			Date returnCommentCreatedDate, String imageURL) {
		this.returnComment = returnComment;
		this.returnCommentBy = returnCommentBy;
		this.returnCommentDescription = returnCommentDescription;
		this.returnCommentCreatedDate = returnCommentCreatedDate;
		this.imageURL = imageURL;
	}

	public String getReturnComment() {
		return returnComment;
	}

	public void setReturnComment(String returnComment) {
		this.returnComment = returnComment;
	}

	public String getReturnCommentBy() {
		return returnCommentBy;
	}

	public void setReturnCommentBy(String returnCommentBy) {
		this.returnCommentBy = returnCommentBy;
	}

	public String getReturnCommentDescription() {
		return returnCommentDescription;
	}

	public void setReturnCommentDescription(String returnCommentDescription) {
		this.returnCommentDescription = returnCommentDescription;
	}

	public Date getReturnCommentCreatedDate() {
		return returnCommentCreatedDate;
	}

	public void setReturnCommentCreatedDate(Date returnCommentCreatedDate) {
		this.returnCommentCreatedDate = returnCommentCreatedDate;
	}

	public String getImageURL() {
		return imageURL;
	}

	public void setImageURL(String imageURL) {
		this.imageURL = imageURL;
	}

	@Override
	public String toString() {
		return "ReturnCommentEntry [returnComment=" + returnComment + ", returnCommentBy=" + returnCommentBy
				+ ", returnCommentDescription=" + returnCommentDescription + ", returnCommentCreatedDate="
				+ returnCommentCreatedDate + ", imageURL=" + imageURL + "]";
	}
}

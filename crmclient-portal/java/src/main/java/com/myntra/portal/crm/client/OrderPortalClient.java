package com.myntra.portal.crm.client;

import com.myntra.commons.client.BaseWebClient;
import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.test.TestHelper;
import com.myntra.commons.utils.ResponseHandler;
import com.myntra.portal.crm.client.constant.CRMPortalClientConstants;
import com.myntra.portal.crm.client.response.CodOnHoldLogResponse;
import com.myntra.portal.crm.client.response.CustomerOrderCommentResponse;
import com.myntra.portal.crm.client.response.OrderExchangeResponse;
import com.myntra.portal.crm.client.response.OrderPaymentLogResponse;
import com.myntra.portal.crm.client.response.OrderRtoResponse;

/**
 * Web client for customer order related detail such as comments, payment logs
 * etc..
 * 
 * @author Arun Kumar
 */
public class OrderPortalClient extends ResponseHandler {

	public static final String SERVICE_PREFIX = "/crm/order/";

	// client api to call payment log service
	public static OrderPaymentLogResponse getPaymentLog(String serviceURL, Long orderId) throws ERPServiceException {

		BaseWebClient client = new BaseWebClient(serviceURL, CRMPortalClientConstants.CRM_PORTAL_URL,
				TestHelper.dummyContextInfo1);
		client.path(SERVICE_PREFIX + "payment/log/{orderId}", orderId);

		return client.get(OrderPaymentLogResponse.class);
	}

	// client api to call comment log service
	public static CustomerOrderCommentResponse getCommentLog(String serviceURL, Long orderId)
			throws ERPServiceException {

		BaseWebClient client = new BaseWebClient(serviceURL, CRMPortalClientConstants.CRM_PORTAL_URL,
				TestHelper.dummyContextInfo1);
		client.path(SERVICE_PREFIX + "comment/log/{orderId}", orderId);

		return client.get(CustomerOrderCommentResponse.class);
	}

	// client api to call COD order onhold log service
	public static CodOnHoldLogResponse getCodOnHoldLog(String serviceURL, Long orderId) throws ERPServiceException {

		BaseWebClient client = new BaseWebClient(serviceURL, CRMPortalClientConstants.CRM_PORTAL_URL,
				TestHelper.dummyContextInfo1);
		client.path(SERVICE_PREFIX + "cod/onhold/log/{orderId}", orderId);

		return client.get(CodOnHoldLogResponse.class);
	}

	// client api to call COD order onhold log service
	public static OrderExchangeResponse getExchangeDetail(String serviceURL, Long orderId) throws ERPServiceException {

		BaseWebClient client = new BaseWebClient(serviceURL, CRMPortalClientConstants.CRM_PORTAL_URL,
				TestHelper.dummyContextInfo1);
		client.path(SERVICE_PREFIX + "exchange/{orderId}", orderId);

		return client.get(OrderExchangeResponse.class);
	}

	// client api to check RTO order or not
	public static OrderRtoResponse getRtoOrderDetail(String serviceURL, Long orderId) throws ERPServiceException {

		BaseWebClient client = new BaseWebClient(serviceURL, CRMPortalClientConstants.CRM_PORTAL_URL,
				TestHelper.dummyContextInfo1);
		
		client.path(SERVICE_PREFIX + "RTO/{orderId}",orderId);	

		return client.get(OrderRtoResponse.class);		
	}

	public static void main(String args[]) throws ERPServiceException {

		/*
		 * // order payment log service test OrderPaymentLogResponse
		 * paymentResponse =
		 * getPaymentLog("http://localhost:7092/crmservice-portal", 4307330L);
		 * System.out.println("\n-----------------------------\n");
		 * System.out.println(paymentResponse);
		 * 
		 * // order comment log service test CustomerOrderCommentResponse
		 * commentResponse =
		 * getCommentLog("http://localhost:7092/crmservice-portal", 4307330L);
		 * System.out.println("\n-----------------------------\n");
		 * System.out.println(commentResponse);
		 */

		// cod order on hold log service test
		/*
		 * CodOnHoldLogResponse onHoldResponse =
		 * getCodOnHoldLog("http://localhost:7092/crmservice-portal",
		 * 65962859L); System.out.println("\n-----------------------------\n");
		 * System.out.println(onHoldResponse);
		 */

		/*
		 * OrderExchangeResponse exchangeResponse =
		 * getExchangeDetail("http://localhost:7092/crmservice-portal",
		 * 7443429L); System.out.println(exchangeResponse);
		 */
		
		// order RTO test 7631271
		OrderRtoResponse rtoResponse = getRtoOrderDetail("http://localhost:7092/crmservice-portal", 4058446L);
		System.out.println("\n-----------------------------\n");
		System.out.println(rtoResponse);
				 
		
	}
}
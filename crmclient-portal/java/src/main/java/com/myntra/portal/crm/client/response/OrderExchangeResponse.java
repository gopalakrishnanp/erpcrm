package com.myntra.portal.crm.client.response;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.myntra.commons.response.AbstractResponse;
import com.myntra.portal.crm.client.entry.OrderExchangeEntry;

/**
 * Response for customer order exchange.
 * 
 * @author Arun Kumar
 */
@XmlRootElement(name = "orderExchangeResponse")
public class OrderExchangeResponse extends AbstractResponse {

	private static final long serialVersionUID = 1L;
	private List<OrderExchangeEntry> orderExchangeEntryList;

	public OrderExchangeResponse() {
		super();
	}

	@XmlElementWrapper(name = "orderExchangeEntryList")
	@XmlElement(name = "orderExchangeEntry")
	public List<OrderExchangeEntry> getOrderExchangeEntryList() {
		return orderExchangeEntryList;
	}

	public void setOrderExchangeEntryList(List<OrderExchangeEntry> orderExchangeEntryList) {
		this.orderExchangeEntryList = orderExchangeEntryList;
	}

	@Override
	public String toString() {
		return "OrderExchangeResponse [orderExchangeEntryList="
				+ orderExchangeEntryList + "]";
	}
}
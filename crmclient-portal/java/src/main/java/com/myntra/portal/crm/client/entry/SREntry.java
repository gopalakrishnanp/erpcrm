package com.myntra.portal.crm.client.entry;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

import com.myntra.commons.entries.BaseEntry;

/**
 * Entry for SR
 * 
 * @author Arun Kumar
 */
@XmlRootElement(name = "SREntry")
public class SREntry extends BaseEntry {

	private static final long serialVersionUID = 1L;

	private String customerEmail;
	private Long orderId;
	private Long SRId;
	private Integer categoryId;
	private String category;
	private Integer subCategoryId;
	private String subCategory;
	private Integer priorityId;
	private String priority;
	private Integer statusId;
	private String status;
	private String department;
	private String channel;
	private Date callBackDate;
	private Date createDate;
	private Date dueDate;
	private Date closeDate;
	private Integer TAT;
	private String reporter;
	private String assignee;
	private String updatedBy;
	private String deletedBy;
	private String resolver;
	private String createSummary;
	private String closeSummary;
	private Long nonOrderSRId;
	private Long orderSRId;

	public SREntry() {
	}

	public SREntry(String customerEmail, Long orderId, Long sRId, Integer categoryId, String category,
			Integer subCategoryId, String subCategory, Integer priorityId, String priority, Integer statusId,
			String status, String department, String channel, Date callBackDate, Date createDate, Date dueDate,
			Date closeDate, Integer tAT, String reporter, String assignee, String updatedBy, String deletedBy,
			String resolver, String createSummary, String closeSummary, Long nonOrderSRId, Long orderSRId) {
		this.customerEmail = customerEmail;
		this.orderId = orderId;
		this.SRId = sRId;
		this.categoryId = categoryId;
		this.category = category;
		this.subCategoryId = subCategoryId;
		this.subCategory = subCategory;
		this.priorityId = priorityId;
		this.priority = priority;
		this.statusId = statusId;
		this.status = status;
		this.department = department;
		this.channel = channel;
		this.callBackDate = callBackDate;
		this.createDate = createDate;
		this.dueDate = dueDate;
		this.closeDate = closeDate;
		this.TAT = tAT;
		this.reporter = reporter;
		this.assignee = assignee;
		this.updatedBy = updatedBy;
		this.deletedBy = deletedBy;
		this.resolver = resolver;
		this.createSummary = createSummary;
		this.closeSummary = closeSummary;
		this.nonOrderSRId = nonOrderSRId;
		this.orderSRId = orderSRId;
	}

	public String getCustomerEmail() {
		return customerEmail;
	}

	public Long getOrderId() {
		return orderId;
	}

	public Long getSRId() {
		return SRId;
	}

	public Integer getCategoryId() {
		return categoryId;
	}

	public String getCategory() {
		return category;
	}

	public Integer getSubCategoryId() {
		return subCategoryId;
	}

	public String getSubCategory() {
		return subCategory;
	}

	public Integer getPriorityId() {
		return priorityId;
	}

	public String getPriority() {
		return priority;
	}

	public Integer getStatusId() {
		return statusId;
	}

	public String getStatus() {
		return status;
	}

	public String getDepartment() {
		return department;
	}

	public String getChannel() {
		return channel;
	}

	public Date getCallBackDate() {
		return callBackDate;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public Date getDueDate() {
		return dueDate;
	}

	public Date getCloseDate() {
		return closeDate;
	}

	public Integer getTAT() {
		return TAT;
	}

	public String getReporter() {
		return reporter;
	}

	public String getAssignee() {
		return assignee;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public String getDeletedBy() {
		return deletedBy;
	}

	public String getResolver() {
		return resolver;
	}

	public String getCreateSummary() {
		return createSummary;
	}

	public String getCloseSummary() {
		return closeSummary;
	}

	public Long getNonOrderSRId() {
		return nonOrderSRId;
	}

	public Long getOrderSRId() {
		return orderSRId;
	}

	public void setCustomerEmail(String customerEmail) {
		this.customerEmail = customerEmail;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public void setSRId(Long sRId) {
		SRId = sRId;
	}

	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public void setSubCategoryId(Integer subCategoryId) {
		this.subCategoryId = subCategoryId;
	}

	public void setSubCategory(String subCategory) {
		this.subCategory = subCategory;
	}

	public void setPriorityId(Integer priorityId) {
		this.priorityId = priorityId;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	public void setStatusId(Integer statusId) {
		this.statusId = statusId;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public void setCallBackDate(Date callBackDate) {
		this.callBackDate = callBackDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}

	public void setCloseDate(Date closeDate) {
		this.closeDate = closeDate;
	}

	public void setTAT(Integer tAT) {
		TAT = tAT;
	}

	public void setReporter(String reporter) {
		this.reporter = reporter;
	}

	public void setAssignee(String assignee) {
		this.assignee = assignee;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public void setDeletedBy(String deletedBy) {
		this.deletedBy = deletedBy;
	}

	public void setResolver(String resolver) {
		this.resolver = resolver;
	}

	public void setCreateSummary(String createSummary) {
		this.createSummary = createSummary;
	}

	public void setCloseSummary(String closeSummary) {
		this.closeSummary = closeSummary;
	}

	public void setNonOrderSRId(Long nonOrderSRId) {
		this.nonOrderSRId = nonOrderSRId;
	}

	public void setOrderSRId(Long orderSRId) {
		this.orderSRId = orderSRId;
	}

	@Override
	public String toString() {
		return "SREntry [customerEmail=" + customerEmail + ", orderId=" + orderId + ", SRId=" + SRId + ", categoryId="
				+ categoryId + ", category=" + category + ", subCategoryId=" + subCategoryId + ", subCategory="
				+ subCategory + ", priorityId=" + priorityId + ", priority=" + priority + ", statusId=" + statusId
				+ ", status=" + status + ", department=" + department + ", channel=" + channel + ", callBackDate="
				+ callBackDate + ", createDate=" + createDate + ", dueDate=" + dueDate + ", closeDate=" + closeDate
				+ ", TAT=" + TAT + ", reporter=" + reporter + ", assignee=" + assignee + ", updatedBy=" + updatedBy
				+ ", deletedBy=" + deletedBy + ", resolver=" + resolver + ", createSummary=" + createSummary
				+ ", closeSummary=" + closeSummary + ", nonOrderSRId=" + nonOrderSRId + ", orderSRId=" + orderSRId
				+ "]";
	}

}

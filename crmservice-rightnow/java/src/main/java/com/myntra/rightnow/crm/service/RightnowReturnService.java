package com.myntra.rightnow.crm.service;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.response.AbstractResponse;
import com.myntra.commons.service.BaseService;
import com.myntra.erp.crm.client.entry.CustomerReturnEntry;
import com.myntra.erp.crm.client.response.CustomerReturnResponse;

/**
 * Customer return search web service interface(abstract)
 * 
 * @author Arun Kumar
 */
@Path("/return/")
public interface RightnowReturnService extends BaseService<CustomerReturnResponse, CustomerReturnEntry> {

	@GET
	@Produces({ "application/xml", "application/json" })
	@Consumes({ "application/xml", "application/json" })
	@Path("/")
	AbstractResponse getReturnDetail(@QueryParam("returnId") Long returnId, @QueryParam("orderId") Long orderId,
			@QueryParam("login") String login,
			@DefaultValue("true") @QueryParam("isLogisticDetailNeeded") boolean isLogisticDetailNeeded)
			throws ERPServiceException;

}

package com.myntra.rightnow.crm.service;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import org.apache.cxf.jaxrs.ext.multipart.MultipartBody;

import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.response.AbstractResponse;
import com.myntra.commons.service.BaseService;
import com.myntra.lms.client.response.OrderEntry;
import com.myntra.rightnow.crm.client.entry.RightnowFileAttachmentEntry;
import com.myntra.rightnow.crm.client.entry.RightnowTaskEntry;
import com.myntra.rightnow.crm.client.entry.RightnowTaskIdEntry;
import com.myntra.rightnow.crm.client.response.RightnowBaseTaskResponse;
import com.myntra.rightnow.crm.client.response.RightnowTaskResponse;

/**
 * task web service interface(abstract)
 * 
 * @author Arun Kumar
 */
@Path("/task/")
public interface RightnowTaskService extends BaseService<RightnowTaskResponse, RightnowTaskEntry> {

	@PUT
	@Produces({ "application/xml", "application/json" })
	@Consumes({ "application/xml", "application/json" })
	@Path("/updateTask")
	public AbstractResponse updateTasks(RightnowTaskResponse taskResponse) throws ERPServiceException;

	@GET
	@Produces({ "application/xml", "application/json" })
	@Consumes({ "application/xml", "application/json" })
	@Path("/note/{taskId}")
	public AbstractResponse getTaskNotes(@PathParam("taskId") Long taskId) throws ERPServiceException;

	@PUT
	@Produces({ "application/xml", "application/json" })
	@Path("/note/list")
	public AbstractResponse getListOfTaskNotes(RightnowTaskResponse taskResponse) throws ERPServiceException;

	@GET
	@Produces({ "application/xml", "application/json" })
	@Consumes({ "application/xml", "application/json" })
	@Path("/attachment")
	public AbstractResponse getTaskAttachments(@QueryParam("taskId") Long taskId) throws ERPServiceException;

	@PUT
	@Produces({ "application/xml", "application/json" })
	@Path("/originalAttachment")
	public AbstractResponse getOriginalAttachment(RightnowFileAttachmentEntry fileEntry) throws ERPServiceException;

	@GET
	@Produces({ "application/xml", "application/json" })
	@Consumes({ "application/xml", "application/json" })
	@Path("/detail/{taskId}")
	public AbstractResponse getTaskDetails(@PathParam("taskId") Long taskId) throws ERPServiceException;

	@PUT
	@Produces({ "application/xml", "application/json" })
	@Path("/noteFromReport")
	public AbstractResponse getRightnowTaskNotesFromReport(RightnowTaskIdEntry taskIDEntry) throws ERPServiceException;

	@POST
	@Produces({ "application/xml", "application/json" })
	@Path("/createTask")
	public AbstractResponse createTasks(RightnowBaseTaskResponse baseTaskResponse) throws ERPServiceException;
	
    @POST
    @Produces({ "application/xml", "application/json" })
    @Consumes({ "application/xml", "application/json", "multipart/form-data" })
    @Path("/upload/attachment")
    public AbstractResponse uploadAttachment(MultipartBody body) throws ERPServiceException;
    
    
    @PUT
	@Produces({ "application/xml", "application/json" })
	@Path("/closeTasks")
	public AbstractResponse closeTasks(RightnowTaskIdEntry taskIDEntry) throws ERPServiceException;
    
    @GET
    @Produces({ "application/xml", "application/json" })
    @Path("/fixTaskCategory")
    public AbstractResponse fixTasksWithNoCategories() throws ERPServiceException;
    
    @POST
	@Produces({ "application/xml", "application/json" })
	@Path("/welcome_calling")
	public AbstractResponse createWelcomeCallTask(OrderEntry lmsOrderEntry) throws ERPServiceException;
}

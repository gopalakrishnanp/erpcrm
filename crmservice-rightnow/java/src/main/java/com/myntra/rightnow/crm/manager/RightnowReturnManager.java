package com.myntra.rightnow.crm.manager;

import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.manager.BaseManager;
import com.myntra.erp.crm.client.entry.CustomerReturnEntry;
import com.myntra.erp.crm.client.response.CustomerReturnResponse;

/**
 * Manager interface(abstract) for customer return service
 * 
 * @author Arun Kumar
 */
public interface RightnowReturnManager extends BaseManager<CustomerReturnResponse, CustomerReturnEntry> {

	public CustomerReturnResponse getReturnDetail(Long returnId, Long orderId, String login,
			boolean isLogisticDetailNeeded) throws ERPServiceException;

}
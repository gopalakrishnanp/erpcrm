package com.myntra.rightnow.crm.service;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.response.AbstractResponse;
import com.myntra.commons.service.BaseService;
import com.myntra.rightnow.crm.client.entry.RightnowReportEntry;
import com.myntra.rightnow.crm.client.response.RightnowReportResponse;

/**
 * report web service interface(abstract)
 * 
 * @author Arun Kumar
 */
@Path("/report/")
public interface RightnowReportService extends BaseService<RightnowReportResponse, RightnowReportEntry> {

	@GET
	@Produces({ "application/xml", "application/json" })
	@Consumes({ "application/xml", "application/json" })
	@Path("/{reportName}")
	AbstractResponse getReportData(@PathParam("reportName") String reportName,
			@QueryParam("startDate") String startDate, @QueryParam("endDate") String endDate)
			throws ERPServiceException;

	@PUT
	@Produces({ "application/xml", "application/json" })
	@Path("/filter/{reportName}/")
	AbstractResponse getReportData(@PathParam("reportName") String reportName, RightnowReportResponse reportResponse)
			throws ERPServiceException;

}

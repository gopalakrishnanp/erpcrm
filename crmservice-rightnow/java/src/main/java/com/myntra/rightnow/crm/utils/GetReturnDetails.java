package com.myntra.rightnow.crm.utils;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;

import org.apache.log4j.Logger;

import com.myntra.erp.crm.client.CustomerReturnClient;
import com.myntra.erp.crm.client.entry.CustomerReturnEntry;
import com.myntra.erp.crm.client.response.CustomerReturnResponse;

public class GetReturnDetails implements Callable<Map<Long, CustomerReturnEntry>> {
	
	private static final Logger LOGGER = Logger.getLogger(GetReturnDetails.class);

	private Long returnID;

	
	public GetReturnDetails(Long returnID) {
		this.returnID = returnID;
	}

	@Override
	public Map<Long, CustomerReturnEntry> call() throws Exception {
		LOGGER.debug(Thread.currentThread().getName() + " Started for fetching return " + returnID + " details");
		Map<Long, CustomerReturnEntry> returnMap = new HashMap<Long, CustomerReturnEntry>();

		Calendar c1 = Calendar.getInstance();
		long t1 = c1.getTimeInMillis();

		CustomerReturnResponse returnResponse = CustomerReturnClient.getReturnById(null, returnID);

		Calendar c2 = Calendar.getInstance();
		long t2 = c2.getTimeInMillis();

		LOGGER.debug("Time Taken by Thread " + Thread.currentThread().getName() + ": " + ((t2-t1)/1000) + " seconds");

		if(returnResponse.getStatus().getStatusType().equals("SUCCESS")
				&& returnResponse.getCustomerReturnEntryList().size()>0) {
			CustomerReturnEntry returnEntry = returnResponse.getCustomerReturnEntryList().get(0);
			
			returnMap.put(returnID, returnEntry);
		}
		
		LOGGER.debug(Thread.currentThread().getName() + " Finished for fetching return " + returnID + " details");
		return returnMap;
	}
}

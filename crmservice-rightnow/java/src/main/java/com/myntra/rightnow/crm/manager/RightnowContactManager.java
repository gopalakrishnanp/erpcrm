package com.myntra.rightnow.crm.manager;

import java.util.Date;

import com.myntra.commons.exception.ERPServiceException;
import com.rightnow.ws.objects.Contact;

public interface RightnowContactManager {

	public boolean createContact(String firstName, String lastName, String gender, String email, String login, String mobile, String phone, Date dob) throws ERPServiceException;
	
	public Contact buildRightnowContactObject(String firstName, String lastName, String gender, String email, String login, String mobile, String phone, Date dob);
}

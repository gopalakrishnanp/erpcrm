package com.myntra.drishti.crm.manager;

import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.manager.BaseManager;
import com.myntra.drishti.crm.client.entry.DrishtiOrderEntry;
import com.myntra.drishti.crm.client.response.DrishtiIvrOrderTestResponse;

public interface DrishtiIvrOrderTestManager extends BaseManager<DrishtiIvrOrderTestResponse, DrishtiOrderEntry> {

	public DrishtiIvrOrderTestResponse getIvrOrderTestDetailsForDrishti(int count, String date) throws ERPServiceException;

}
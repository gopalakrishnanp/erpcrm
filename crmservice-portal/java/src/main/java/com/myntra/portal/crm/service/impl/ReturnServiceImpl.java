package com.myntra.portal.crm.service.impl;

import org.springframework.transaction.TransactionSystemException;

import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.response.AbstractResponse;
import com.myntra.commons.service.impl.BaseServiceImpl;
import com.myntra.portal.crm.client.entry.ReturnEntry;
import com.myntra.portal.crm.client.response.ReturnResponse;
import com.myntra.portal.crm.manager.ReturnManager;
import com.myntra.portal.crm.service.ReturnService;

/**
 * Customer return web service implementation which integrates detail of return
 * and comments
 * 
 * @author Arun Kumar
 */
public class ReturnServiceImpl extends BaseServiceImpl<ReturnResponse, ReturnEntry> implements ReturnService {

	@Override
	public AbstractResponse getReturnDetail(Long returnId, Long orderId, String login) {
		try {
			return ((ReturnManager) getManager()).getReturnDetail(returnId, orderId, login);

		} catch (ERPServiceException e) {
			return e.getResponse();

		} catch (TransactionSystemException ex) {
			return ((ERPServiceException) ex.getApplicationException()).getResponse();
		}

	}

}

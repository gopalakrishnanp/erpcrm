package com.myntra.portal.crm.manager.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.myntra.commons.codes.StatusResponse;
import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.manager.impl.BaseManagerImpl;
import com.myntra.commons.response.AbstractResponse;
import com.myntra.portal.crm.client.code.CRMPortalSuccessCodes;
import com.myntra.portal.crm.client.entry.OrderExchangeEntry;
import com.myntra.portal.crm.client.response.OrderExchangeResponse;
import com.myntra.portal.crm.dao.CustomerOrderDAO;
import com.myntra.portal.crm.entity.OrderExchangeEntity;
import com.myntra.portal.crm.manager.OrderExchangeManager;

/**
 * Order Exchange Manager Implement for getting details of exchange for an orderid
 * 
 * @author Pravin Mehta
 */
public class OrderExchangeManagerImpl extends BaseManagerImpl<OrderExchangeResponse, OrderExchangeEntry> implements OrderExchangeManager {

	private static final Logger LOGGER = Logger.getLogger(OrderExchangeManagerImpl.class);
	
	private CustomerOrderDAO customerOrderDAO;

	public CustomerOrderDAO getCustomerOrderDAO() {
		return customerOrderDAO;
	}

	public void setCustomerOrderDAO(CustomerOrderDAO customerOrderDAO) {
		this.customerOrderDAO = customerOrderDAO;
	}
	
	@Override
	public OrderExchangeResponse getExchangeDetail(Long orderId) throws ERPServiceException {

		// Initialise order exchange response and entry
		OrderExchangeResponse response = new OrderExchangeResponse();
		List<OrderExchangeEntry> custOrderExchangeEntryList = new ArrayList<OrderExchangeEntry>();

		try {
			List<OrderExchangeEntity> orderExchangeEntityList = (List<OrderExchangeEntity>) ((CustomerOrderDAO) getCustomerOrderDAO()).getExchangeDetail(orderId);
			if (orderExchangeEntityList != null) {
				for (OrderExchangeEntity singleOrderExchangeEntity : orderExchangeEntityList) {
					OrderExchangeEntry singleOrderExchangeEntry = toCustomerOrderExchangeEntry(singleOrderExchangeEntity);
					custOrderExchangeEntryList.add(singleOrderExchangeEntry);
				}
			}
			
			response.setOrderExchangeEntryList(custOrderExchangeEntryList);

		} catch (ERPServiceException e) {
			LOGGER.error("Error validating/retreiving data", e);
			StatusResponse error = new StatusResponse(e.getCode(), e.getMessage(), StatusResponse.Type.ERROR);
			response.setStatus(error);
			throw new ERPServiceException(response);

		} catch (Exception e) {
			LOGGER.error("error", e);
			StatusResponse error = new StatusResponse(1, e.getMessage(), StatusResponse.Type.ERROR);
			response.setStatus(error);
			throw new ERPServiceException(response);
		}

		StatusResponse success = new StatusResponse(CRMPortalSuccessCodes.EXCHANGE_DETAIL_RETRIEVED,
				StatusResponse.Type.SUCCESS, response.getOrderExchangeEntryList().size());
		response.setStatus(success);

		return response;
	}

	private OrderExchangeEntry toCustomerOrderExchangeEntry(OrderExchangeEntity oxe) {

		if (oxe == null) {
			return null;
		}

		return new OrderExchangeEntry(oxe.getExchangeOrderId(), oxe.getShipmentId(),
				oxe.getItemId(), oxe.getReturnId(), oxe.getReturnType());
	}

	@Override
	public AbstractResponse findById(Long id) throws ERPServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AbstractResponse create(OrderExchangeEntry entry)
			throws ERPServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AbstractResponse update(OrderExchangeEntry entry, Long itemId)
			throws ERPServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AbstractResponse search(int start, int fetchSize, String sortBy,
			String sortOrder, String searchTerms) throws ERPServiceException {
		// TODO Auto-generated method stub
		return null;
	}
}
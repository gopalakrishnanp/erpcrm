package com.myntra.portal.crm.service.impl;

import org.springframework.transaction.TransactionSystemException;

import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.response.AbstractResponse;
import com.myntra.commons.service.impl.BaseServiceImpl;
import com.myntra.portal.crm.client.entry.OrderPaymentLogEntry;
import com.myntra.portal.crm.client.response.OrderPaymentLogResponse;
import com.myntra.portal.crm.manager.OrderPaymentLogManager;
import com.myntra.portal.crm.service.OrderPaymentLogService;

/**
 * Order payment log service interface to retrieve the detail of order payment
 * and gateway
 * 
 * @author Arun Kumar
 */
public class OrderPaymentLogServiceImpl extends BaseServiceImpl<OrderPaymentLogResponse, OrderPaymentLogEntry>
		implements OrderPaymentLogService {

	@Override
	public AbstractResponse getOrderPaymentLog(Long orderId) {
		try {
			return ((OrderPaymentLogManager) getManager()).getOrderPaymentLog(orderId);

		} catch (ERPServiceException e) {
			return e.getResponse();

		} catch (TransactionSystemException ex) {
			return ((ERPServiceException) ex.getApplicationException()).getResponse();
		}

	}
}

package com.myntra.portal.crm.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Query;

import org.apache.log4j.Logger;

import com.myntra.commons.dao.impl.BaseDAOImpl;
import com.myntra.commons.exception.ERPServiceException;
import com.myntra.portal.crm.constant.CRMPortalServiceConstants;
import com.myntra.portal.crm.dao.SRDAO;
import com.myntra.portal.crm.entity.SREntity;

/**
 * DAO for SR detail
 * 
 * @author Arun Kumar
 */
public class SRDAOImpl extends BaseDAOImpl<SREntity> implements SRDAO {

	//private static final Logger LOGGER = Logger.getLogger(SRDAOImpl.class);

	@Override
	public List<SREntity> getSRDetail(Long id, String loginOrMobile, String filterType) throws ERPServiceException {

		Query q = null;

		if (filterType.equalsIgnoreCase("order")) {
			q = getEntityManager().createNamedQuery(CRMPortalServiceConstants.NAMED_QUERIES.SR_BY_ORDERID);
			q.setParameter("orderId", id);

		} else if (filterType.equalsIgnoreCase("SR")) {
			q = getEntityManager().createNamedQuery(CRMPortalServiceConstants.NAMED_QUERIES.SR_BY_SRID);
			q.setParameter("SRId", id);

		} else if (filterType.equalsIgnoreCase("login")) {
			q = getEntityManager().createNamedQuery(CRMPortalServiceConstants.NAMED_QUERIES.SR_BY_LOGIN);
			q.setParameter("login", loginOrMobile);
			
		} else if (filterType.equalsIgnoreCase("mobile")) {
			q = getEntityManager().createNamedQuery(CRMPortalServiceConstants.NAMED_QUERIES.SR_BY_MOBILE);
			q.setParameter("mobile", loginOrMobile);
		}

		List<Object[]> list = q.getResultList();
		List<SREntity> srEntityList = new ArrayList<SREntity>();

		for (Object[] objArray : list) {
			int index = 0;

			String email = (String) (objArray[index++]);
			Long oId = null;
			Integer oIdInteger = (Integer) (objArray[index++]);
			if (oIdInteger != null) {
				oId = Long.valueOf(oIdInteger);
			}

			Long SRId = null;
			Integer SRIdInteger = (Integer) (objArray[index++]);
			if (SRIdInteger != null) {
				SRId = Long.valueOf(SRIdInteger);
			}

			Integer categoryId = (Integer) (objArray[index++]);
			String category = (String) (objArray[index++]);
			Integer subCategoryId = (Integer) (objArray[index++]);
			String subCategory = (String) objArray[index++];

			Integer priorityId = null;

			Byte priorityIdByte = (Byte) (objArray[index++]);
			if (priorityIdByte != null) {
				priorityId = priorityIdByte.intValue();
			}

			String priority = (String) objArray[index++];

			Integer statusId = null;
			Byte statusIdByte = (Byte) (objArray[index++]);
			if (statusIdByte != null) {
				statusId = statusIdByte.intValue();
			}

			String status = (String) objArray[index++];
			String department = (String) objArray[index++];
			String channel = (String) objArray[index++];

			Date callBackDate = null;
			Integer callBackInteger = (Integer) objArray[index++];
			if (callBackInteger != null) {
				callBackDate = new Date(callBackInteger * 1000L);
			}

			Date createdDate = null;
			Integer createdInteger = (Integer) objArray[index++];
			if (createdInteger != null) {
				createdDate = new Date(createdInteger * 1000L);
			}

			Date dueDate = null;
			Integer dueInteger = (Integer) objArray[index++];
			if (dueInteger != null) {
				dueDate = new Date(dueInteger * 1000L);
			}

			Date closedDate = null;
			Integer closedInteger = (Integer) objArray[index++];
			if (closedInteger != null) {
				closedDate = new Date(closedInteger * 1000L);
			}

			Integer TAT = (Integer) objArray[index++];
			String reporter = (String) objArray[index++];
			String assignee = (String) objArray[index++];
			String updatedBy = (String) objArray[index++];
			String deletedBy = (String) objArray[index++];
			String resolver = (String) objArray[index++];
			String createSummary = (String) objArray[index++];
			String closeSummary = (String) objArray[index++];

			Long nonOrderSRId = null;
			Integer nonOrderSRIdInteger = (Integer) (objArray[index++]);
			if (nonOrderSRIdInteger != null) {
				nonOrderSRId = Long.valueOf(nonOrderSRIdInteger);
			}

			Long orderSRId = null;
			Integer orderSRIdInteger = (Integer) (objArray[index++]);
			if (orderSRIdInteger != null) {
				orderSRId = Long.valueOf(orderSRIdInteger);
			}

			SREntity entity = new SREntity(email, oId, SRId, categoryId, category, subCategoryId, subCategory,
					priorityId, priority, statusId, status, department, channel, callBackDate, createdDate, dueDate,
					closedDate, TAT, reporter, assignee, updatedBy, deletedBy, resolver, createSummary, closeSummary,
					nonOrderSRId, orderSRId);

			srEntityList.add(entity);
			Logger.getLogger(this.getClass()).debug("CheckSREntity : " + entity.toString());
		}
		return srEntityList;
	}
}

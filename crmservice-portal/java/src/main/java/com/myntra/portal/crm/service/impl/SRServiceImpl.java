package com.myntra.portal.crm.service.impl;

import org.springframework.transaction.TransactionSystemException;

import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.response.AbstractResponse;
import com.myntra.commons.service.impl.BaseServiceImpl;
import com.myntra.portal.crm.client.entry.SREntry;
import com.myntra.portal.crm.client.response.SRResponse;
import com.myntra.portal.crm.manager.SRManager;
import com.myntra.portal.crm.service.SRService;

/**
 * to get Service Request(SR) detail
 * 
 * @author Arun Kumar
 */
public class SRServiceImpl extends BaseServiceImpl<SRResponse, SREntry> implements SRService {

	@Override
	public AbstractResponse getSRDetailById(Long SRId) throws ERPServiceException {
		try {
			return ((SRManager) getManager()).getSRDetail(SRId, null, "SR");

		} catch (ERPServiceException e) {
			return e.getResponse();

		} catch (TransactionSystemException ex) {
			return ((ERPServiceException) ex.getApplicationException()).getResponse();
		}
	}

	@Override
	public AbstractResponse getSRDetailByOrder(Long orderId) throws ERPServiceException {
		try {
			return ((SRManager) getManager()).getSRDetail(orderId, null, "order");

		} catch (ERPServiceException e) {
			return e.getResponse();

		} catch (TransactionSystemException ex) {
			return ((ERPServiceException) ex.getApplicationException()).getResponse();
		}
	}

	@Override
	public AbstractResponse getSRDetailByLoginOrMobile(String loginOrMobile) throws ERPServiceException {
		try {
			return ((SRManager) getManager()).getSRDetail(null, loginOrMobile, "loginOrMobile");

		} catch (ERPServiceException e) {
			return e.getResponse();

		} catch (TransactionSystemException ex) {
			return ((ERPServiceException) ex.getApplicationException()).getResponse();
		}
	}
}
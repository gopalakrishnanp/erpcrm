package com.myntra.portal.crm.entity;

import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;

import com.myntra.commons.entities.BaseEntity;
import com.myntra.portal.crm.constant.CRMPortalServiceConstants;

/**
 * Entity for order RTO detail
 * 
 * @author Arun Kumar
 */
@Entity
@Table(name = "mk_old_returns_tracking")
@NamedNativeQuery(
		name = CRMPortalServiceConstants.NAMED_QUERIES.RTO_BY_ORDERID, 
		query = "select orderid, " +
				" elt(currstate,'RTOQ','RTOCAL1','RTOCAL2','RTOCAL3','RTOC','RTORS','RTORSC','RTORF','RTOAI','RTQ','RTCSD','RTPI','RTPTU','RTR','RTQP','RTQF','RTCC','RTQPA','RTQPNA','RTRF','RTAI','RTRS','RTRSC','END','CMMNT','RTCL','RTOCL','RTOPREQ') " +
				" as rtostatus " +
				" from mk_old_returns_tracking " + 
				" where orderid=:orderId and returntype='RTO'", 
		resultSetMapping = CRMPortalServiceConstants.SQL_RESULT_SET_MAP.MAP_RTO_BY_ORDERID
	)

@SqlResultSetMapping(
		name = CRMPortalServiceConstants.SQL_RESULT_SET_MAP.MAP_RTO_BY_ORDERID, 
		columns = {
				@ColumnResult(name = "orderId"), 
				@ColumnResult(name = "rtoStatus")
		})

public class OrderRtoEntity extends BaseEntity {

	private static final long serialVersionUID = 3688398701763484215L;	

	@Id
	@Column(name = "orderId")
	private Long orderId;

	@Column(name = "rtoStatus")
	private String rtoStatus;

	public OrderRtoEntity() {

	}

	public OrderRtoEntity(Long orderId, String rtoStatus) {
		this.orderId = orderId;
		this.rtoStatus = rtoStatus;
	}

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public String getRtoStatus() {
		return rtoStatus;
	}

	public void setRtoStatus(String rtoStatus) {
		this.rtoStatus = rtoStatus;
	}

	@Override
	public String toString() {
		return "OrderRtoEntity [orderId=" + orderId + ", rtoStatus=" + rtoStatus + "]";
	}

}
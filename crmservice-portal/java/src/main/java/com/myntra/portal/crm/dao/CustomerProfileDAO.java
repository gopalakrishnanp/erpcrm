package com.myntra.portal.crm.dao;

import java.util.List;

import com.myntra.commons.dao.BaseDAO;
import com.myntra.commons.exception.ERPServiceException;
import com.myntra.portal.crm.entity.CustomerProfileEntity;

public interface CustomerProfileDAO extends BaseDAO<CustomerProfileEntity> {
    
    public List<CustomerProfileEntity> getCustomerProfileByLogin(String login) throws ERPServiceException;
    public List<CustomerProfileEntity> getCustomerProfileByMobile(String mobile) throws ERPServiceException;
    public boolean createCustomerProfile() throws ERPServiceException;
}

package com.myntra.portal.crm.dao;

import java.util.List;

import com.myntra.commons.dao.BaseDAO;
import com.myntra.commons.exception.ERPServiceException;
import com.myntra.portal.crm.entity.CouponEntity;

public interface CouponDAO extends BaseDAO<CouponEntity> {
    
    public List<CouponEntity> getCouponForLogin(String login) throws ERPServiceException;

}

package com.myntra.portal.crm.manager.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.myntra.commons.codes.StatusResponse;
import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.manager.impl.BaseManagerImpl;
import com.myntra.commons.response.AbstractResponse;
import com.myntra.portal.crm.client.code.CRMPortalSuccessCodes;
import com.myntra.portal.crm.client.entry.CustomerOrderCommentEntry;
import com.myntra.portal.crm.client.response.CustomerOrderCommentResponse;
import com.myntra.portal.crm.dao.CustomerOrderDAO;
import com.myntra.portal.crm.entity.CustomerOrderCommentEntity;
import com.myntra.portal.crm.manager.OrderCommentLogManager;

/**
 * Manager implementation for order comment log.
 * 
 * @author Arun Kumar
 */
public class OrderCommentLogManagerImpl extends
		BaseManagerImpl<CustomerOrderCommentResponse, CustomerOrderCommentEntry> implements OrderCommentLogManager {

	private static final Logger LOGGER = Logger.getLogger(OrderCommentLogManagerImpl.class);

	private CustomerOrderDAO customerOrderDAO;

	public CustomerOrderDAO getCustomerOrderDAO() {
		return customerOrderDAO;
	}

	public void setCustomerOrderDAO(CustomerOrderDAO customerOrderDAO) {
		this.customerOrderDAO = customerOrderDAO;
	}

	@Override
	public CustomerOrderCommentResponse getOrderCommentLog(Long orderId) throws ERPServiceException {

		// Initialise order Comment response and entry
		CustomerOrderCommentResponse response = new CustomerOrderCommentResponse();

		List<CustomerOrderCommentEntry> customerOrderCommentEntryList = new ArrayList<CustomerOrderCommentEntry>();

		try {

			List<CustomerOrderCommentEntity> customerOrderCommentEntityList = (List<CustomerOrderCommentEntity>) ((CustomerOrderDAO) getCustomerOrderDAO())
					.getCustomerOrderCommentLog(orderId);

			if (customerOrderCommentEntityList != null) {
				for (CustomerOrderCommentEntity singleOrderCommentEntity : customerOrderCommentEntityList) {
					CustomerOrderCommentEntry singleOrderCommentEntry = toCustomerOrderCommentEntry(singleOrderCommentEntity);
					customerOrderCommentEntryList.add(singleOrderCommentEntry);
				}
			}
			// set the entry list even if the size of the list is zero
			response.setCustomerOrderCommentEntryList(customerOrderCommentEntryList);

		} catch (ERPServiceException e) {
			LOGGER.error("Error validating/retreiving data", e);
			StatusResponse error = new StatusResponse(e.getCode(), e.getMessage(), StatusResponse.Type.ERROR);
			response.setStatus(error);
			throw new ERPServiceException(response);

		} catch (Exception e) {
			LOGGER.error("error", e);
			StatusResponse error = new StatusResponse(1, e.getMessage(), StatusResponse.Type.ERROR);
			response.setStatus(error);
			throw new ERPServiceException(response);
		}

		StatusResponse success = new StatusResponse(CRMPortalSuccessCodes.ORDER_COMMENT_LOG_RETRIEVED,
				StatusResponse.Type.SUCCESS, response.getCustomerOrderCommentEntryList().size());
		response.setStatus(success);

		return response;
	}

	private CustomerOrderCommentEntry toCustomerOrderCommentEntry(CustomerOrderCommentEntity coce) {

		if (coce == null) {
			return null;
		}

		return new CustomerOrderCommentEntry(coce.getCommentId(), coce.getOrderId(), coce.getType(), coce.getTitle(),
				coce.getCommentBy(), coce.getDescription(), coce.getNewAddress(), coce.getGiftWrapMessage(),
				coce.getCommentDate(), coce.getAttachmentName());
	}

	@Override
	public AbstractResponse findById(Long id) throws ERPServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AbstractResponse search(int start, int fetchSize, String sortBy, String sortOrder, String searchTerms)
			throws ERPServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AbstractResponse create(CustomerOrderCommentEntry entry) throws ERPServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AbstractResponse update(CustomerOrderCommentEntry entry, Long itemId) throws ERPServiceException {
		// TODO Auto-generated method stub
		return null;
	}
}
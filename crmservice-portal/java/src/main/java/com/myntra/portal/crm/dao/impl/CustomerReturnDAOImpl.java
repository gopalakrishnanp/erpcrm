package com.myntra.portal.crm.dao.impl;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Query;

import com.myntra.commons.dao.impl.BaseDAOImpl;
import com.myntra.commons.exception.ERPServiceException;
import com.myntra.portal.crm.constant.CRMPortalServiceConstants;
import com.myntra.portal.crm.dao.CustomerReturnDAO;
import com.myntra.portal.crm.entity.CustomerReturnCommentEntity;
import com.myntra.portal.crm.entity.CustomerReturnEntity;

/**
 * DAO for customer return detail
 * 
 * @author Arun Kumar
 */
public class CustomerReturnDAOImpl extends BaseDAOImpl<CustomerReturnEntity> implements CustomerReturnDAO {

	//private static final Logger LOGGER = Logger.getLogger(CustomerReturnDAOImpl.class);

	@Override
	public List<CustomerReturnEntity> getCustomerReturnData(Long returnId, Long orderId, String login)
			throws ERPServiceException {

		if ((login == null || login == "") && returnId == null && orderId == null) {
			throw new ERPServiceException("Please provide return id or order id or email of the customer", 0);
		}

		Query q = null;

		if (returnId != null) {
			q = getEntityManager().createNamedQuery(CRMPortalServiceConstants.NAMED_QUERIES.CUSTOMER_RETURN_BY_RETURNID);
			q.setParameter("returnid", returnId);

		} else if (orderId != null) {
			q = getEntityManager().createNamedQuery(CRMPortalServiceConstants.NAMED_QUERIES.CUSTOMER_RETURN_BY_ORDERID);
			q.setParameter("orderid", orderId);

		} else if (login != null && login != "") {
			q = getEntityManager().createNamedQuery(CRMPortalServiceConstants.NAMED_QUERIES.CUSTOMER_RETURN_BY_LOGIN);
			q.setParameter("login", login);
		}

		List<Object[]> list = q.getResultList();
		List<CustomerReturnEntity> customerReturnEntityList = new ArrayList<CustomerReturnEntity>();

		for (Object[] objArray : list) {
			int index = 0;

			Long rId = null;
			Long oId = null;
			Integer rIdInteger = (Integer) (objArray[index++]);
			Integer oIdInteger = (Integer) (objArray[index++]);

			if (rIdInteger != null) {
				rId = Long.valueOf(rIdInteger);
			}

			if (oIdInteger != null) {
				oId = Long.valueOf(oIdInteger);
			}
			
			Long itemId = null;
			Integer itemIdInteger = (Integer) (objArray[index++]);
			if (itemIdInteger != null) {
				itemId = Long.valueOf(itemIdInteger);
			}

			String skuCode = (String) objArray[index++];
			Integer quantity = (Integer) objArray[index++];
			String size = (String) objArray[index++];
			String returnStatus = (String) objArray[index++];
			String returnLogin = (String) objArray[index++];
			String customerName = (String) objArray[index++];
			String returnAddress = (String) objArray[index++];
			String city = (String) objArray[index++];
			String state = (String) objArray[index++];
			String country = (String) objArray[index++];
			String zipCode = (String) objArray[index++];

			String mobile = (String) objArray[index++];
			String returnMode = (String) objArray[index++];
			String courierService = (String) objArray[index++];
			String trackingNumber = (String) objArray[index++];
			String returnReason = (String) objArray[index++];
			String returnDescription = (String) objArray[index++];

			Double pickupCharge = null;
			BigDecimal pickupChargeDecimal = (BigDecimal) objArray[index++];
			if (pickupChargeDecimal != null) {
				pickupCharge = pickupChargeDecimal.doubleValue();
			}

			Double refundAmount = null;
			BigDecimal refundAmountDecimal = (BigDecimal) objArray[index++];
			if (refundAmountDecimal != null) {
				refundAmount = refundAmountDecimal.doubleValue();
			}

			Date returnCreatedDate = null;
			Timestamp returnCreatedDateTime = (Timestamp) objArray[index++];
			if (returnCreatedDateTime != null) {
				returnCreatedDate = new Date(returnCreatedDateTime.getTime());
			}

			String dCCode = (String) objArray[index++];
			Boolean isRefunded = (Boolean) objArray[index++];

			Date refundDate = null;
			Timestamp refundDateTime = (Timestamp) objArray[index++];
			if (refundDateTime != null) {
				refundDate = new Date(refundDateTime.getTime());
			}

			Double refundModeCredit = null;
			BigDecimal refundModeCreditDecimal = (BigDecimal) objArray[index++];
			if (refundModeCreditDecimal != null) {
				refundModeCredit = refundModeCreditDecimal.doubleValue();
			}

			String returnStatusCode = (String) objArray[index++];
			String returnStatusName = (String) objArray[index++];
			String returnStatusDescription = (String) objArray[index++];
			String itemBarCode = (String) objArray[index++];
			Integer wareHouseId = (Integer) objArray[index++];

			CustomerReturnEntity entity = new CustomerReturnEntity(rId, oId, itemId, skuCode, quantity, size, returnStatus,
					returnLogin, customerName, returnAddress, city, state, country, zipCode, mobile, returnMode,
					courierService, trackingNumber, returnReason, returnDescription, pickupCharge, refundAmount,
					returnCreatedDate, dCCode, isRefunded, refundDate, refundModeCredit, returnStatusCode,
					returnStatusName, returnStatusDescription, itemBarCode, wareHouseId);

			customerReturnEntityList.add(entity);		

		}

		return customerReturnEntityList;
	}

	@Override
	public List<CustomerReturnCommentEntity> getCustomerReturnCommentData(Long returnId) throws ERPServiceException {
		if (returnId == null) {
			throw new ERPServiceException("Return Id for comment is null.", 0);
		}

		Query q = null;
		q = getEntityManager().createNamedQuery(CRMPortalServiceConstants.NAMED_QUERIES.CUSTOMER_RETURN_COMMENT_BY_RETURNID);
		q.setParameter("returnid", returnId);

		List<Object[]> list = q.getResultList();

		List<CustomerReturnCommentEntity> customerReturnCommentEntityList = new ArrayList<CustomerReturnCommentEntity>();

		for (Object[] objArray : list) {

			int index = 0;

			String returnComment = (String) objArray[index++];
			String returnCommentBy = (String) objArray[index++];
			String returnCommentDescription = (String) objArray[index++];

			Date returnCommentCreatedDate = null;
			Timestamp returnCommentCreatedDateTime = (Timestamp) objArray[index++];
			if (returnCommentCreatedDateTime != null) {
				returnCommentCreatedDate = new Date(returnCommentCreatedDateTime.getTime());
			}

			String returnImageURL = (String) objArray[index++];

			CustomerReturnCommentEntity customerReturnCommentEntity = new CustomerReturnCommentEntity(returnComment,
					returnCommentBy, returnCommentDescription, returnCommentCreatedDate, returnImageURL);

			customerReturnCommentEntityList.add(customerReturnCommentEntity);
		}

		return customerReturnCommentEntityList;
	}
}

package com.myntra.portal.crm.manager.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.myntra.commons.codes.StatusResponse;
import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.manager.impl.BaseManagerImpl;
import com.myntra.commons.response.AbstractResponse;
import com.myntra.portal.crm.client.code.CRMPortalSuccessCodes;
import com.myntra.portal.crm.client.entry.OrderRtoEntry;
import com.myntra.portal.crm.client.response.OrderRtoResponse;
import com.myntra.portal.crm.dao.CustomerOrderDAO;
import com.myntra.portal.crm.entity.OrderRtoEntity;
import com.myntra.portal.crm.manager.OrderRtoManager;

/**
 * Manager implementation for order RTO detail.
 * 
 * @author Arun Kumar
 */
public class OrderRtoManagerImpl extends BaseManagerImpl<OrderRtoResponse, OrderRtoEntry> implements OrderRtoManager {

	private static final Logger LOGGER = Logger.getLogger(OrderRtoManagerImpl.class);

	private CustomerOrderDAO customerOrderDAO;

	public CustomerOrderDAO getCustomerOrderDAO() {
		return customerOrderDAO;
	}

	public void setCustomerOrderDAO(CustomerOrderDAO customerOrderDAO) {
		this.customerOrderDAO = customerOrderDAO;
	}

	public OrderRtoManagerImpl() {
	}

	@Override
	public OrderRtoResponse getOrderRtoDetail(Long orderId) throws ERPServiceException {

		OrderRtoResponse response = new OrderRtoResponse();

		List<OrderRtoEntry> orderRtoEntryList = new ArrayList<OrderRtoEntry>();

		try {

			List<OrderRtoEntity> orderRtoEntityList = (List<OrderRtoEntity>) ((CustomerOrderDAO) getCustomerOrderDAO())
					.getOrderRtoDetail(orderId);

			if (orderRtoEntityList != null) {
				for (OrderRtoEntity singleOrderRtoEntity : orderRtoEntityList) {
					OrderRtoEntry singleOrderRtoEntry = toOrderRtoEntry(singleOrderRtoEntity);
					orderRtoEntryList.add(singleOrderRtoEntry);
				}
			}
			// set the entry list even if the size of the list is zero
			response.setOrderRtoEntryList(orderRtoEntryList);

		} catch (ERPServiceException e) {
			LOGGER.error("Error validating/retreiving data", e);
			StatusResponse error = new StatusResponse(e.getCode(), e.getMessage(), StatusResponse.Type.ERROR);
			response.setStatus(error);
			throw new ERPServiceException(response);

		} catch (Exception e) {
			LOGGER.error("error", e);
			StatusResponse error = new StatusResponse(1, e.getMessage(), StatusResponse.Type.ERROR);
			response.setStatus(error);
			throw new ERPServiceException(response);
		}

		StatusResponse success = new StatusResponse(CRMPortalSuccessCodes.RTO_RETRIEVED, StatusResponse.Type.SUCCESS,
				response.getOrderRtoEntryList().size());
		response.setStatus(success);

		return response;
	}

	private OrderRtoEntry toOrderRtoEntry(OrderRtoEntity or) {

		if (or == null) {
			return null;
		}

		OrderRtoEntry rtoEntry = new OrderRtoEntry();
		Map<String, String> rtoStatusMap = rtoEntry.getRtoStatusMap();
		String rtoStatusDisplay = rtoStatusMap.get(or.getRtoStatus());

		rtoEntry.setOrderId(or.getOrderId());
		rtoEntry.setRtoStatus(or.getRtoStatus());
		rtoEntry.setRtoStatusDisplay(rtoStatusDisplay);
		return rtoEntry;
	}

	@Override
	public AbstractResponse findById(Long id) throws ERPServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AbstractResponse search(int start, int fetchSize, String sortBy, String sortOrder, String searchTerms)
			throws ERPServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AbstractResponse create(OrderRtoEntry entry) throws ERPServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AbstractResponse update(OrderRtoEntry entry, Long itemId) throws ERPServiceException {
		// TODO Auto-generated method stub
		return null;
	}
}